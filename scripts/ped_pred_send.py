#!/usr/bin/env python
import rospy
import time

from ford_msgs.msg import PedTrajVec
from ford_msgs.msg import PedTraj
from ford_msgs.msg import Pose2DStamped
from geometry_msgs.msg import Pose2D
from geometry_msgs.msg import Point32
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import os
import pickle
import numpy as np
import math

# set random seed
np.random.seed(0)

# publish origin to raven_rviz
def publish_origin_axis():
    x_marker = Marker()
    x_marker.header.frame_id = "/world"
    x_marker.header.stamp = rospy.Time.now()
    x_marker.ns = "axis"
    x_marker.id = 0
    x_marker.action = Marker.ADD
    x_marker.lifetime = rospy.Duration(0)
    x_marker.type = Marker.LINE_STRIP;
    x_marker.pose.orientation.w = 1.0
    x_marker.color.a = 1.0
    x_marker.color.r = 1.0
    x_marker.scale.x = 0.1
    x_marker.points.append(Point32(0.0, 0.0, 0.0))
    x_marker.points.append(Point32(1.0, 0.0, 0.0))

    y_marker = Marker()
    y_marker.header.frame_id = "/world"
    y_marker.header.stamp = rospy.Time.now()
    y_marker.ns = "axis"
    y_marker.id = 1
    y_marker.action = Marker.ADD
    y_marker.lifetime = rospy.Duration(0)
    y_marker.type = Marker.LINE_STRIP;
    y_marker.pose.orientation.w = 1.0
    y_marker.color.a = 1.0
    y_marker.color.g = 1.0
    y_marker.scale.x = 0.1
    y_marker.points.append(Point32(0.0, 0.0, 0.0))
    y_marker.points.append(Point32(0.0, 1.0, 0.0))

    return x_marker, y_marker

# plot trajectories in dark grey as background (typically the training set)
def trajs2MarkerArray(trajs):
    counter = 0
    marker_array = MarkerArray()

    print len(trajs)
    rospy.sleep(2)

    for traj in trajs:
        
        x = traj[:, 0]
        y = traj[:, 1]

        marker = Marker()
        marker.header.frame_id = "/world"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "background"
        marker.id = counter
        marker.action = Marker.ADD
        marker.lifetime = rospy.Duration(0)
        marker.type = Marker.LINE_STRIP
        marker.pose.orientation.w = 0.3
        marker.color.a = 0.4
        marker.color.r = 0.4
        marker.color.b = 0.4
        marker.color.g = 0.4
        marker.scale.x = 0.05

        counter += 1
        for j in xrange(len(x)):
            marker.points.append(Point32(x[j], y[j], -0.05))

        marker_array.markers.append(marker)
    return marker_array

# plot trajectories in dark grey as background (typically the training set)
def timeMarker():
    marker = Marker()
    marker.header.frame_id = "/world"
    marker.header.stamp = rospy.Time.now()
    marker.ns = "timer"
    marker.id = 1
    marker.action = Marker.ADD
    marker.lifetime = rospy.Duration(0)
    marker.type = Marker.TEXT_VIEW_FACING
    marker.text = 'time: 0.0 s'
    marker.pose.position.x = -8
    marker.pose.position.y = -8
    marker.pose.position.z = 0

    marker.color.a = 1.0
    marker.color.r = 1.0
    marker.color.b = 1.0
    marker.color.g = 1.0
    marker.scale.z = 1.5
    return marker



# simulated circular path
class SimPath:
    def __init__(self, center, radius, v, id):

        self.id = id
        self.center = center
        self.radius = radius
        self.v = v
        self.cur_angle = 2 * math.pi * np.random.rand(1) # random starting point
        self.active_time = 10 + (30 - 10) * np.random.rand(1)
        self.last_update_time = time.time()
        self.start_time = time.time()
        self.ped_traj_msg = PedTraj()
        self.ped_traj_msg.ped_id = id
        self.type = 'SimPath'


        # update marker
        self.marker = Marker()
        self.marker.header.frame_id = "/world"
        self.marker.header.stamp = rospy.Time.now()
        self.marker.ns = "obs"
        self.marker.id = id
        self.marker.action = Marker.ADD;
        self.marker.lifetime = rospy.Duration(10)
        self.marker.type = Marker.LINE_STRIP;
        self.marker.pose.orientation.w = 1.0
        self.marker.color.a = 1.0
        self.marker.color.r = 1.0
        self.marker.color.b = 1.0
        self.marker.color.g = 1.0
        self.marker.scale.x = 0.1

        # update current position
        self.update_cur_pos()

    def if_active(self):
        return (time.time()-self.start_time) < self.active_time

    def update_cur_pos(self):
        dt = time.time() - self.last_update_time
        self.cur_angle += dt * self.v / self.radius;
        x_offset = self.radius * math.cos(self.cur_angle); 
        y_offset = self.radius * math.sin(self.cur_angle);
        self.cur_pos = self.center + np.array([x_offset, y_offset])
        new_point = Pose2DStamped()
        new_point.header.frame_id ='map'
        new_point.header.stamp = rospy.Time.now()
        new_point.pose.x = self.cur_pos[0]
        new_point.pose.y = self.cur_pos[1]
        self.ped_traj_msg.traj.append(new_point)


        self.marker.points.append(Point32(self.cur_pos[0], self.cur_pos[1], 0.0))
        self.last_update_time = time.time()





# prescribed path
# typically loaded from a text file
class ReplayPath:
    def __init__(self, traj, id):
        self.id = id
        self.x = traj[:,0] 
        self.y = traj[:,1]
        self.dx_dt = traj[:,2]
        self.dy_dt = traj[:,3]
        self.dt = traj[:,4]
        self.ifComplete = False
        self.updateTotalTime()
        self.cur_ind = 0
        self.start_time = time.time()
        self.ped_traj_msg = PedTraj()
        self.ped_traj_msg.ped_id = id
        self.type = 'ReplayPath'
        self.ros_prev_time = rospy.Time.now()


        # update marker
        self.marker = Marker()
        self.marker.header.frame_id = "/world";
        self.marker.header.stamp = rospy.Time.now()
        self.marker.ns = "obs";
        self.marker.id = id;
        self.marker.action = Marker.ADD;
        self.marker.lifetime = rospy.Duration(5)
        self.marker.type = Marker.LINE_STRIP;
        self.marker.pose.orientation.w = 1.0
        self.marker.color.a = 1.0
        self.marker.color.r = 1.0
        self.marker.color.b = 1.0
        self.marker.color.g = 1.0
        self.marker.scale.x = 0.1

        # update current position
        self.update_cur_pos()

    def updateTotalTime(self):
        self.t_total = np.zeros(self.dt.shape, dtype = float)
        self.t_total[0] = self.dt[0]
        for i in xrange(1, self.dt.shape[0]):
            self.t_total[i] = self.t_total[i-1] + self.dt[i] 

    def if_active(self):
        self.ifComplete = (time.time()-self.start_time) > self.t_total[-1]
        return (not self.ifComplete)

    def update_cur_pos(self):
        cur_time = time.time() - self.start_time
        # not yet complete and should publish next point
        if not self.ifComplete and cur_time > self.t_total[self.cur_ind]:
            while cur_time > self.t_total[self.cur_ind]:
                self.cur_pos = np.array([self.x[self.cur_ind], self.y[self.cur_ind]])
                new_point = Pose2DStamped()
                new_point.header.frame_id ='map'
                dt_dur = rospy.Duration(self.dt[self.cur_ind])
                new_point.header.stamp = self.ros_prev_time + dt_dur
                self.ros_prev_time = new_point.header.stamp
                new_point.pose.x = self.cur_pos[0]
                new_point.pose.y = self.cur_pos[1]
                self.ped_traj_msg.traj.append(new_point)
                
                self.marker.points.append(Point32(self.cur_pos[0], self.cur_pos[1], 0.0))

                self.cur_ind +=1
                if self.cur_ind > self.x.shape[0]:
                    self.ifComplete = True                                        


if __name__ == '__main__':
    rospy.init_node('ped_pred_sender',anonymous=False)
    ped_pub = rospy.Publisher('ped_pred/input',PedTrajVec, queue_size=10, latch=True)
    marker_pub = rospy.Publisher('ped_pred/markers', MarkerArray, queue_size = 1, latch=True)
    # rospy.sleep(10)

    start_time = time.time()
    marker_array = MarkerArray()
    ped_trajs_msg = PedTrajVec()
    time_marker = timeMarker()
    marker_array.markers.append(time_marker)
    
    # === Set parameters === #
    rospy.set_param('ped_pred/dt', 0.5)
    rospy.set_param('ped_pred/steps', 10)
    rospy.set_param('ped_pred/prune_time', 1.0)

    # load paths from file
    file_dir = os.path.dirname(os.path.realpath(__file__))
    # location = 'bldg4'
    location = 'stata' 
    # training
    trajs_train = pickle.load(open(file_dir+'/../data_folder/trajs_train_' \
    + location+'.p', 'rb'))
    marker_pub.publish(trajs2MarkerArray(trajs_train))

    # testing
    trajs_test = pickle.load(open(file_dir+'/../data_folder/trajs_test_' \
        + location+'.p', 'rb'))
    num_trajs = len(trajs_test)
    print 'num_trajs', num_trajs
    #rand_inds = np.random.randint(low=0, high=num_trajs-1,size=num_trajs)
    rand_inds = np.random.permutation(num_trajs)
    # rand_inds = np.array([13, 20, 19, 8, 14, 6, 17, 2, 18, 15])
    # rand_inds = np.array([9, 6, 9, 6, 9, 6, 9, 6])
    print rand_inds

    # predestrians
    # sim circular path
    # path1 = SimPath(np.array([0.0,0.0],dtype=float), 3 , 0.5, 1)
    # path2 = SimPath(np.array([0.8,0.8],dtype=float), 3 , 0.5, 2)

    # replay, load from a text file
    global_id = 0
    path1 = ReplayPath(trajs_test[rand_inds[global_id]],rand_inds[global_id])
    global_id += 1;
    path2 = ReplayPath(trajs_test[rand_inds[global_id]],rand_inds[global_id])
    # path2.t_total[-1] = -9999
    global_id += 1;

    # markers
    # x_axis_marker, y_axis_marker = publish_origin_axis()
    # marker_array.markers.append(x_axis_marker)
    # marker_array.markers.append(y_axis_marker)
    marker_array.markers.append(path1.marker)
    marker_array.markers.append(path2.marker)
    ped_trajs_msg.ped_traj_vec.append(path1.ped_traj_msg)
    ped_trajs_msg.ped_traj_vec.append(path2.ped_traj_msg)

    

    ros_rate = rospy.Rate(5)
    #rospy.Timer(rospy.Duration(0.5), call_back)
    while not rospy.is_shutdown():

        if path1.if_active() == False and path2.if_active() == False:
            marker_array.markers = []
            marker_array.markers.append(time_marker)
            ped_trajs_msg.ped_traj_vec = []
            print "both paths are inactive"
            print "publish new paths", global_id+2
            # replay path until no more in the text file
            if path1.type == 'ReplayPath' and global_id < num_trajs:
                path1 = ReplayPath(trajs_test[rand_inds[global_id]],rand_inds[global_id]);
                marker_array.markers.append(path1.marker)
                ped_trajs_msg.ped_traj_vec.append(path1.ped_traj_msg)
                global_id += 1;
            # if path2.type == 'ReplayPath' and global_id < num_trajs:
            #     path2 = ReplayPath(trajs_test[rand_inds[global_id]],rand_inds[global_id]);
            #     marker_array.markers.append(path2.marker)
            #     ped_trajs_msg.ped_traj_vec.append(path2.ped_traj_msg)
            #     global_id += 1;
            if not (path1.type == 'ReplayPath' and path2.type == 'ReplayPath'):
                break
            if (path1.type == 'ReplayPath' and path2.type == 'ReplayPath' and \
                global_id >= num_trajs):
                break;
        print "time elapsed: %fs, traj %d" % \
                (time.time()-start_time, path1.id)


        if path1.if_active() == True:
            path1.update_cur_pos()
            #ped_trajs_msg.ped_traj_vec.append(path1.ped_traj_msg)

        if path2.if_active() == True:
            path2.update_cur_pos()
            # ped_trajs_msg.ped_traj_vec.append(path2.ped_traj_msg)

        # publish and clear
        if len(ped_trajs_msg.ped_traj_vec) > 0:
            #print 'ped_trajs_msg.ped_traj_vec', len(ped_trajs_msg.ped_traj_vec)
            ped_pub.publish(ped_trajs_msg)
            # ped_trajs_msg.ped_traj_vec = []
        
        time_marker.text = 'time: %.1fs' % (time.time() - start_time)
        marker_pub.publish(marker_array)

        ros_rate.sleep()
        #rospy.spin()

    print 'ped_pred_sender exiting'





















