%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For developling GP prediction in C++
%
% Steven Chen, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;

addpath plotting

% parameters for generating observations
n = 30;
x_min = -5; x_max = 5; y_min = -5; y_max = 5;
pLimit = [x_min, x_max, y_min, y_max];
speed = 0.5;

% set hyperparameter
lx = 5;
ly = 5;
sigma_input = 1;
sigma_noise = 0.05;
hyperparam = [lx, ly, sigma_input, sigma_noise];
sparseGP.hyperparam = hyperparam;

% load testCase
load test_case
plot(x,y,'ro');
plotTraj( x, y, dx_dt, dy_dt );

% updating function mean and variance
x_query = x_min: 0.5: x_max;
y_query = y_min: 0.5: y_max;

[X_query,Y_query] = meshgrid(x_query, y_query);


X_query = reshape(X_query,[length(x_query)*length(y_query),1]);
Y_query = reshape(Y_query,[length(x_query)*length(y_query),1]);


% GP prediction
% hyperparam_x = optimize_GP_l(x, y, dx_dt, hyperparam);
% hyperparam_y = optimize_GP_l(x, y, dy_dt, hyperparam);
hyperparam_x = hyperparam;
hyperparam_y = hyperparam;
[x_vel, var_x] = GP_predict(x, y, dx_dt, X_query, Y_query, hyperparam_x);
[y_vel, var_y] = GP_predict(x, y, dy_dt, X_query, Y_query, hyperparam_y);
plotGP( X_query, Y_query, x_vel, y_vel, x, y);

% for testing
x_test = [0, 1,  2, 3];
y_test = [-1,  0, 1, 2];
[x_vel, ~] = GP_predict(x, y, dx_dt, x_test, y_test, hyperparam_x);
[y_vel, ~] = GP_predict(x, y, dy_dt, x_test, y_test, hyperparam_y);
fprintf(sprintf('x \t y \t vx_pred \t vy_pred \n '));
for i = 1:length(x_test)
    fprintf(sprintf('%.2f \t %.2f \t %.2f \t %.2f \n ', x_test(i), y_test(i), x_vel(i), y_vel(i)));
end

% simple test case
x = [-1, 0, 1]';
y = [1, 0.8, 1]';
f = [0.6, 0.2, 0.3]';
x_test = x - 0.1;
y_test = y + 0.1;
%hyperparam = [lx, ly, sigma_input, sigma_noise]
hyperparam = [1, 1, 0.3, 0.1];
[pred_f, ~] = GP_predict(x,y,f,x_test, y_test, hyperparam);
fprintf('simple test case \n pref_f: ');
for i = 1:length(f)
    fprintf(sprintf('\t %f', pred_f(i)));
end
fprintf('\n');
% % sparse GP 
% sparseGP_x = build_sparseGP(x, y, dx_dt, hyperparam);
% sparseGP_y = build_sparseGP(x, y, dy_dt, hyperparam);
% [x_vel_sparse, var_x_sparse] = sparseGP_predict(sparseGP_x, X_query, Y_query);
% [y_vel_sparse, var_y_sparse] = sparseGP_predict(sparseGP_y, X_query, Y_query);
% plotSparseGP( X_query, Y_query, x_vel_sparse, y_vel_sparse, x, y, sparseGP_x, sparseGP_y);
% 
% 
% % debugging sparseGP_predict_distri.m
% pos = [0,0];
% pos_var = [1 0; 0 1];
% [mu_dis, var_dis]= sparseGP_predict_distri(sparseGP_x, pos, pos_var);
% [x_vel_sparse, var_x_sparse] = sparseGP_predict(sparseGP_x, pos(1), pos(2));
% 
% % predicting future trajectory given starting point
% starting_point = [-2,0];
% num_steps = 10;
% dt = ones(num_steps,1);
% pos_var_in = [0.5, 0; 0, 0.5];
% pred_traj = sparseGP_trajPredict(sparseGP_x, sparseGP_y, starting_point, pos_var_in, dt, num_steps, speed);
% predPlot(pred_traj, sparseGP_x, sparseGP_y);