#ifndef LINEARPRED_HPP
#define LINEARPRED_HPP

#include <iostream>
#include <algorithm>
#include <math.h>
#include <Eigen/Dense>
#include <Eigen/LU>

#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )
#define PI 3.14159265

class LinearPred
{
public:
	LinearPred(double dt, int steps){
		this->dt = dt;
		this->steps = Eigen::VectorXd::LinSpaced(steps+1, 0, steps);
		//std::cout << "steps" << std::endl << this->steps << std::endl;
		//std::cout << "constructor returned" << std::endl;
	};
	virtual ~LinearPred(){};

	Eigen::Vector2d build_linear_model(int &num_pts, Eigen::VectorXd x_in, Eigen::VectorXd y_in)
	{
		// std::cout << "build_linear_model started" << std::endl;
		//std::cout << "x\n" << x << std::endl;
		//std::cout << "y\n" << y << std::endl;
		double dist = 0, dist_thres = 4; // prediction relies on traj data from the past 4 meters
		num_pts = x_in.rows();

		for (int i = x_in.rows()-1; i >0; --i)
		{
			dist += sqrt((x_in(i) - x_in(i-1)) * (x_in(i) - x_in(i-1)) 
				         + (y_in(i) - y_in(i-1)) * (y_in(i) - y_in(i-1)));
			if (dist > dist_thres)
			{
				num_pts = (x_in.rows()-1) - i + 2;
				break;
			}
		}

		Eigen::VectorXd x = x_in.tail(num_pts);
		Eigen::VectorXd y = y_in.tail(num_pts);


		//int num_pts = std::min(10, int(x.rows()));
		//if (num_pts == 10)
		//{
		//	x = x.tail(10);
		//	y = y.tail(10);
		//}

		// construct H and W matrixes
		Eigen::MatrixXd W = Eigen::MatrixXd::Identity(num_pts, num_pts);
		//W(num_pts-1, num_pts-1) = 10;

		Eigen::MatrixXd H(num_pts, 2);
		H << x, Eigen::VectorXd::Ones(num_pts);

		// weighted least square regression
		// (H' *W * H) \ ( H' * W * y);
		Eigen::Vector2d param = (H.transpose() * W * H).lu().solve( (H.transpose() * W * y) );
		// std::cout << "num_pts" << num_pts << std::endl;
		// std::cout << "build_linear_model returned\n" << param << std::endl;
		return param;
	};

	Eigen::MatrixXd linear_model_pred(int num_pts, Eigen::VectorXd x_in, Eigen::VectorXd y_in, double v, Eigen::Vector2d param)
	{
		//std::cout << "linear_model_pred 1" << std::endl;
		// int num_pts = std::min(10, int(x.rows()));
		// if (num_pts == 10)
		// {
		// 	x = x.tail(10);
		// 	y = y.tail(10);
		// }
		// std::cout << "in linear_model_pred_1: x_in(end), y_in(end)" << x_in(x_in.rows()-1) << " " << y_in(y_in.rows()-1) << std::endl;
		// std::cout << "num_pts:" << num_pts << "x_in_length: " << x_in.rows() << std::endl;
		Eigen::VectorXd x = x_in.tail(num_pts);
		Eigen::VectorXd y = y_in.tail(num_pts);

		// std::cout << "in linear_model_pred_1.5: x(end), y(end)" << x(x.rows()-1) << " " << y(y.rows()-1) << std::endl;
		// parameters of the line
		double a = param(0);
		double b = param(1);
		//  find out heading direction
		double dir_mag_x = x(x.rows()-1) - x(0);
		double dir_mag_y = y(y.rows()-1) - y(0);
		int dir_sign_x = sign(dir_mag_x);
		int dir_sign_y = sign(dir_mag_y);

		if (fabs(dir_mag_x) > fabs(dir_mag_y))
			dir_sign_y = sign( dir_mag_x * a);
		else
			dir_sign_x = sign( dir_mag_y * a);


		// std::cout << "in linear_make_pred, param\n" << param << std::endl;
		// make prediction
		Eigen::MatrixXd pred = Eigen::MatrixXd::Zero(steps.rows(),2);

		double ds = v * dt;
		double dx = ds * sqrt(1.0 / (1.0 + a * a));
		double dy = ds * fabs(a) * sqrt(1.0 / (1.0 + a * a));

		// find x and y
		pred.col(0) = double(dir_sign_x) * dx * steps;
		pred.col(1) = double(dir_sign_y) * dy * steps;
		pred.col(0).array() += x(x.rows()-1);
		pred.col(1).array() += y(y.rows()-1);

		// double angle = atan2 (double(dir_sign_y) * fabs(a), double(dir_sign_x)) * 180.0 / PI;

		// std::cout << "in linear_model_pred_2: x(end), y(end)" << x(x.rows()-1) << " " << y(y.rows()-1) << std::endl;

		// std::cout << "pred" << std::endl << pred << std::endl;
		//std::cout << "linear_model_pred returned" << std::endl;
		return pred;
	};


	Eigen::MatrixXd  make_pred (Eigen::MatrixXd pos, double v){
		Eigen::VectorXd x = pos.col(0);
		Eigen::VectorXd y = pos.col(1);

		// std::cout << "in make_pred: x(end), y(end)" << x(x.rows()-1) << " " << y(y.rows()-1) << std::endl;
		int num_pts = 0; // pass by reference, value will change
		Eigen::Vector2d param = build_linear_model(num_pts, x, y);
		return linear_model_pred(num_pts, x, y, v, param);
	};


	// for debugging
	Eigen::MatrixXd  get_observed_partial_path (Eigen::MatrixXd pos, double v){
		Eigen::VectorXd x = pos.col(0);
		Eigen::VectorXd y = pos.col(1);

		// std::cout << "in make_pred: x(end), y(end)" << x(x.rows()-1) << " " << y(y.rows()-1) << std::endl;
		int num_pts = 0; // pass by reference, value will change
		Eigen::Vector2d param = build_linear_model(num_pts, x, y);
		Eigen::MatrixXd partial_path = Eigen::MatrixXd::Zero(num_pts,2);
		partial_path.col(0) = x.tail(num_pts);
		// partial_path.col(1) = y.tail(num_pts);
		// std::cout << "param" << param << std::endl;
		partial_path.col(1) = param(0) * x.tail(num_pts).array() + param(1);
		return partial_path;
	};


private:
	double dt;
	Eigen::VectorXd steps;
};

#undef sign
#endif