#include <iostream>
#include "linear_pred.hpp"
#include "gp_pred.hpp"
#include "motion_gp.hpp"
#include <stdlib.h> 
#include <string>
// ros includes
#include <ros/ros.h>
#include <ros/package.h>


int main(int argc, char* argv[])
{
	std::cout << "hello world from debug.cpp in acl_gp" << std::endl;
	srand(1);
	ros::init(argc, argv, "debug");
	std::string gp_filename = "/data_folder/GPs.txt";
	std::string sc_filename = "/data_folder/scBases.txt";
	std::string path = ros::package::getPath("acl_gp");
	double dt = 0.5;
	int steps = 5;
	GP_Pred gp_predictor(path+gp_filename, path+sc_filename, dt, steps);
	ros::spin();

	return 0;
}