#include <ros/ros.h>
#include <iostream>
#include "linear_pred.hpp"
#include "motion_gp.hpp"
#include "pedestrian.hpp"
#include "gp_pred.hpp"

#include <ford_msgs/PedTraj.h>
#include <ford_msgs/PedTrajVec.h>
// #include <ford_msgs/RawTraj.h>
// #include <ford_msgs/Pred_PedTrajs.h>

#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
// #include <acl_gp/PedestrianPath.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/ColorRGBA.h>
#include <string> 
#include <map>
#include <vector>
#include <algorithm>    // std::reverse

// ros includes
#include <ros/ros.h>
#include <ros/package.h>

#define DOWNSAMPLE_DIST 0.5 // min distance between two consecutive points
#define DOWNSAMPLE_TIME 0.5 // max time between two consecutive times
#define VEL_FILTER_TIME 1.3 // average speed from the past 1.3 seconds

void downSampler(const Eigen::VectorXd &x, const Eigen::VectorXd &y, const Eigen::VectorXd &dt, 
    Eigen::VectorXd &x_d, Eigen::VectorXd &y_d, Eigen::VectorXd &dt_d){
    int num_points = x.rows();
    assert(num_points > 1);
    std::vector <int> valid_inds;
    std::vector <double> dt_vec;
    double dist = 0, dt_scalar = 0;
    valid_inds.push_back(num_points-1); // always include the endpoint
    // find valid indices
    // std::cout << "num_points"  << num_points << std::endl;
    for (int i = num_points - 2 ; i>=0; --i){
        // std::cout << "herer i "  << i << std::endl;
        dist += sqrt((x(i+1) - x(i)) * (x(i+1) - x(i)) + (y(i+1)-y(i)) * (y(i+1)-y(i)));
        dt_scalar += dt(i+1);
        if (dist > DOWNSAMPLE_DIST || dt_scalar > DOWNSAMPLE_TIME){
            valid_inds.push_back(i);
            dt_vec.push_back(dt_scalar);
            dist = 0;
            dt_scalar = 0;
        }
    }

    std::reverse(valid_inds.begin(),valid_inds.end());
    std::reverse(dt_vec.begin(),dt_vec.end());
    // populate downsampled vectors
    int ds_pts= valid_inds.size();
    // std::cout << "ds_pts" << ds_pts << std::endl;
    x_d = Eigen::VectorXd::Zero(ds_pts); y_d = Eigen::VectorXd::Zero(ds_pts);
    dt_d = Eigen::VectorXd::Zero(ds_pts);
    for (int i = 0; i < ds_pts; i++){
        x_d(i) = x(valid_inds[i]);
        y_d(i) = y(valid_inds[i]);
        if (i>0)
            dt_d(i) = dt_vec[i-1];
    }
    // std::cout << "downsampling" << std::endl;
    // for (int i = 0; i < ds_pts; i++)
    //      std::cout << "x, y, delta_t(i) " << x_d(i) << " " << y_d(i) << " " << dt_d(i) << std::endl;
};

double velFilter(const Eigen::VectorXd &x, const Eigen::VectorXd &y, const Eigen::VectorXd &dt){
    double dt_scalar = 0, ds_dt_sum = 0;
    int num_points = x.rows();
    double num_used_points = 0;
    assert (num_points > 1);
    for (int i = num_points - 1; i >0; i--){
        dt_scalar += dt(i);
        ds_dt_sum += sqrt(((x(i) - x(i-1))*(x(i) - x(i-1)) + (y(i) - y(i-1)) * (y(i) - y(i-1)))) / dt(i);
        num_used_points += 1;
        if (dt_scalar > VEL_FILTER_TIME)
            break;
    }
    // std::cout << "speed :" << ds_dt_sum / num_used_points << std::endl;
    return ds_dt_sum / num_used_points;
};


class PedPredNode{
public:
    ros::NodeHandle nh_p_;
    ros::Subscriber sub_input_;
    ros::Publisher pub_output_;
    ros::Publisher pub_markers_;

    ros::Timer prune_timer_;

    double dt_;
    int steps_;
    double prune_time_;

    std::map <int, Pedestrian*> pedestrian_map;
    std::string ns; 
    std::vector <std_msgs::ColorRGBA> color_vec;
    GP_Pred* gp_predictor;

    // constructor
    PedPredNode()
    {
        nh_p_ = ros::NodeHandle("~");
        setDefaultParameters();
        getParameters();
        sub_input_ = nh_p_.subscribe("input",1,&PedPredNode::cbPedTrajVec, this);
        ns = "ped_predictor";
        // pub_output_ = nh_p_.advertise<ford_msgs::Pred_PedTrajs>("output",10,true); //true for latching
        pub_output_ = nh_p_.advertise<ford_msgs::PedTrajVec>("ped_pred",10,true); //TODO: determine proper queue size
        pub_markers_ = nh_p_.advertise<visualization_msgs::MarkerArray>("/ped_pred/markers_pred",10,true); //true for latching
        
        ROS_INFO_STREAM("[ped_pred]: Initialized. " << " dt = " << dt_ << 
                        " steps = " << steps_ << " prune_time = " << prune_time_);
        populateColorList();

        // construct gp predictor
        // std::string location = "bldg4";
        std::string location = "stata";
        std::string gp_filename = "/data_folder/GPs_" + location + ".txt";
        std::string sc_filename = "/data_folder/scBases_" + location + ".txt";

        //std::string gp_filename = "/data_folder/GPs.txt";
        //std::string sc_filename = "/data_folder/scBases.txt";
        std::string path = ros::package::getPath("acl_gp");
        gp_predictor = new GP_Pred(path+gp_filename, path+sc_filename, dt_, steps_);

        // === Call prune_inactive regularily === //
        prune_timer_ = nh_p_.createTimer(ros::Duration(prune_time_),&PedPredNode::cbPrune,this);

    };
    //destructor
    ~PedPredNode(){
        // free list of pedestrians
        std::map<int,Pedestrian*>::iterator it;
        for (it = pedestrian_map.begin(); it != pedestrian_map.end();)
        {
            delete it->second;
            pedestrian_map.erase(it++);
        }
        // free gp_predictor
        delete gp_predictor;
    }

    void populateColorList()
    {
        // colors for publishing                            
        std_msgs::ColorRGBA color_null; // blue for now, gray later
        color_null.b = 0.3;
        color_null.g = 0.3;
        color_null.r = 0.3;
        color_null.a = 1.0;
        std_msgs::ColorRGBA color_gp1; // red
        color_gp1.r = 1.0;
        color_gp1.a = 1.0;
        std_msgs::ColorRGBA color_gp2; // lighter red
        color_gp2.r = 0.6;
        color_gp2.a = 1.0;
        std_msgs::ColorRGBA color_gp3; // lightest red
        color_gp3.r = 210.0 / 255.0;
        color_gp3.g = 105.0 / 255.0;
        color_gp3.b = 30.0 / 255.0;
        color_gp3.a = 0.5;
        std_msgs::ColorRGBA color_linear; // green
        color_linear.g = 1.0;
        color_linear.a = 1.0;
        std_msgs::ColorRGBA color_observed; // blue for now, gray later
        color_observed.b = 1.0;
        color_observed.g = 1.0;
        color_observed.a = 1.0;

        color_vec.push_back(color_null); // type = 0
        color_vec.push_back(color_gp1);       // type = GP1 = 1
        color_vec.push_back(color_gp2);       // type = GP2 = 2
        color_vec.push_back(color_gp3);       // type = GP3 = 3
        color_vec.push_back(color_linear);   // type = Linear = 4
        color_vec.push_back(color_observed); // type = observed_path = 5
    }


    // set paramaters for prediction
    void setDefaultParameters()
    {
        if (!ros::param::has("~dt")) { ros::param::set("~dt",0.5);}
        if (!ros::param::has("~steps")) { ros::param::set("~steps",10);}
        if (!ros::param::has("~prune_time_")) { ros::param::set("~prune_time_",1.0);}
    }

    // get parameters for prediction
    void getParameters()
    {
        ros::param::getCached("~dt",dt_);
        ros::param::getCached("~steps",steps_);
        ros::param::getCached("~prune_time",prune_time_);
    }

    // find the pedestrian with id
    // if id doesn't exist, create a new instance 
    Pedestrian* findPedestrian(int id)
    {
        std::map<int,Pedestrian*>::iterator it = pedestrian_map.find(id);
        if (it!=pedestrian_map.end())
        {
            return it->second;
        }
        else
        {
            // create new instance and insert to map
            Pedestrian* new_ped = new Pedestrian(dt_, steps_, id, ns+to_string(id), gp_predictor, ros::Time::now().toSec());
            pedestrian_map[id] = new_ped;
            return new_ped;
        }
    };

    // prune inactive pedestrians
    void prune_inactive(){
        std::map<int,Pedestrian*>::iterator it;
        int counter = 0;
        double current_time = ros::Time::now().toSec();
        for (it=pedestrian_map.begin(); it!=pedestrian_map.end();)
        {
            if (it->second->if_active(current_time) == false){
                delete it->second;
                pedestrian_map.erase(it++);
            }
            else{
                ++it;
            }
            counter ++;
        }
        return;
    };

    // callback
    void  cbPrune(const ros::TimerEvent& timerEvent)
    {
        prune_inactive();
    };

    void cbPedTrajVec(const ford_msgs::PedTrajVec& pedTrajVec)
    {
        std::vector<ford_msgs::PedTraj>::const_iterator it;
        for (it = pedTrajVec.ped_traj_vec.begin(); it != pedTrajVec.ped_traj_vec.end(); ++it){
            cbPedTraj(*it);
        }
    };

    void cbPedTraj(const ford_msgs::PedTraj& pedTraj)
    {
        ROS_INFO_STREAM("[ped_pred]: new point received");
        //std::cout << "traj id:" << pedTraj.ped_id <<std::endl;
        getParameters();
        int num_of_points = pedTraj.traj.size();

        // if empty msg
        if (num_of_points < 2)
            return;
        Pedestrian* ped = findPedestrian(pedTraj.ped_id);
        
        // === Construct pos Matrix === //
        Eigen::VectorXd x(num_of_points),y(num_of_points), delta_t(num_of_points);
        delta_t(0) = 0;
        for (int i = 0; i < num_of_points; i++){
            x(i) = pedTraj.traj[i].pose.x;
            y(i) = pedTraj.traj[i].pose.y;
            if (i < num_of_points -1)
                delta_t(i+1) = (pedTraj.traj[i+1].header.stamp - pedTraj.traj[i].header.stamp).toSec();
            // std::cout << "x, y, delta_t(i) " << x(i) << " " << y(i) << " " << delta_t(i) << std::endl;
        }

        // Eigen::MatrixXd pos(x.rows(),2);
        // pos << x, y;

        // downsampling
        Eigen::VectorXd x_d, y_d, delta_t_d;
        downSampler(x, y, delta_t, x_d, y_d, delta_t_d);

        Eigen::MatrixXd pos(x_d.rows(),2);
        pos << x_d, y_d;




        // === Make Prediction === //
        double v = 1.0; //TODO: estimate v using the trajectory
        double v_est_x = pedTraj.traj.back().velocity.x;
        double v_est_y = pedTraj.traj.back().velocity.y;
        v = sqrt(v_est_x*v_est_x + v_est_y*v_est_y);
        if (v < 1e-3)
        {
            if (x_d.rows() > 1){
                v = velFilter(x_d, y_d, delta_t_d);
                //std::cout << "filtered v down_sampled" << velFilter(x_d, y_d, delta_t_d) << std::endl;
            }
            else{
                v = velFilter(x, y, delta_t);
                //std::cout << "filtered v " << velFilter(x, y, delta_t) << std::endl;
            }
        }
        

        // predict paths
        ped->make_pred(pos, v, ros::Time::now().toSec());
        
        // visualization 
        visualization_msgs::MarkerArray marker_array = toTrajMarkerArray(ped);
        pub_markers_.publish(marker_array);

        // publish path
        // ford_msgs::Pred_PedTrajs pred_pedtrajs= toPred_PedTrajs(ped, pedTraj.traj.back().header.stamp);
        
        ford_msgs::PedTrajVec pred_pedtrajs = toPred_PedTrajs(*ped, pedTraj.traj.back().header.stamp, pedTraj.traj.back().header.frame_id);
        pub_output_.publish(pred_pedtrajs);
    };

    // void cbInput(const acl_gp::PedestrianPath& input_msg)
    // {
    //     // TODO read dt and steps as parameters
    //     // ROS_INFO_STREAM("[ped_pred]: new point received");
    //     getParameters();
    //     // x << 1.1, 1.2, 1.4;
    //     // y << 0.1, 0.15, 0.21;
    //     int num_of_points = input_msg.path.points.size();
    //     // if empty msg
    //     if (num_of_points == 0)
    //         return;

    //     Pedestrian* ped = findPedestrian(input_msg.id);


    //     // === Construct pos Matrix === //
    //     Eigen::VectorXd x(num_of_points),y(num_of_points);
    //     for (int i = 0; i < num_of_points; i++){
    //         x(i) = input_msg.path.points[i].x;
    //         y(i) = input_msg.path.points[i].y;
    //     }
    //     Eigen::MatrixXd pos(x.rows(),2);
    //     pos << x, y;

    //     // === Make Prediction === //
    //     ped->make_pred(pos, input_msg.v);
        
    //     /*
    //     acl_gp::PedestrianPath output_msg;
    //     output_msg.v = input_msg.v;
    //     output_msg.id = input_msg.id;
    //     //geometry_msgs::Polygon output_msg;
    //     for (int i = 0; i < predMatrix.rows(); i++){
    //         geometry_msgs::Point32 point;
    //         point.x = predMatrix.row(i)[0];
    //         point.y = predMatrix.row(i)[1];
    //         output_msg.path.points.push_back(point);
    //     }
    //     pub_output_.publish(output_msg);
    //     ROS_INFO_STREAM("[linear_pred]: output " << output_msg);
    //     */

    //     // === Publish Markers === //
    //     visualization_msgs::MarkerArray marker_array = toTrajMarkerArray(ped);
    //     pub_markers_.publish(marker_array);
    // }

    // populate a structure for publishing the set of predicted paths
    ford_msgs::PedTrajVec toPred_PedTrajs(const Pedestrian& ped, const ros::Time meas_time, const std::string& frame_id){
        // ford_msgs::Pred_PedTrajs pred_pedtrajs;
        // pred_pedtrajs.ped_id = ped->id;
        ford_msgs::PedTrajVec pedTrajVec;

        /* Compute sum of likelihood to normalize for likelihood value*/
        // double likelihood_sum = 0;
        // for (std::vector<double>::const_iterator double_it = ped.pred_likelihood.begin(); double_it != ped.pred_likelihood.end(); ++double_it){
        //     likelihood_sum += *double_it;
        // }

        for (size_t i = 0; i < ped.pred_paths.size(); ++i) {
            // ford_msgs::RawTraj pred_traj;
            ford_msgs::PedTraj pred_traj;
            pred_traj.ped_id = ped.id;

            /* Set trajectory type according to prediction type*/
            int pred_type = ped.pred_type[i];
            if (pred_type == 1 || pred_type == 2 || pred_type == 3){
                // GP_TYPE
                pred_traj.type = ford_msgs::PedTraj::PREDICTION_GP;
                pred_traj.value = (double) (4 - pred_type);
            }
            else if (pred_type == 4){
                pred_traj.type = ford_msgs::PedTraj::PREDICTION_LINEAR;
            }

            for (size_t j = 0; j < ped.pred_paths[i].rows(); ++j){
                ford_msgs::Pose2DStamped pt;
                pt.header.stamp = meas_time+ ros::Duration(ped.dt_ * j);
                pt.header.frame_id = frame_id;
                pt.pose.x = ped.pred_paths[i](j,0);
                pt.pose.y = ped.pred_paths[i](j,1);
                pred_traj.traj.push_back(pt);
            }
            /*Set pred_traj.value according to likelihood*/
            // if (likelihood_sum > 0 && i < ped.pred_likelihood.size()){
                // pred_traj.value = ped.pred_likelihood[i]/likelihood_sum;
            // }

            pedTrajVec.ped_traj_vec.push_back(pred_traj);
        }
        return pedTrajVec;
    };

    // visualization -- create marker array
    visualization_msgs::MarkerArray toTrajMarkerArray(const Pedestrian* ped){
        visualization_msgs::MarkerArray marker_array;

        // loop through number of predicted paths
        for (size_t i = 0; i < ped->pred_paths.size(); ++i){
                marker_array.markers.push_back(toTrajMarker(ped->pred_paths[i],ped->ns, i, ped->pred_type[i]));
            }

        // clear previous paths
        if (ped->pred_paths.size() < ped->prev_num_active_ids)
            for (int i = ped->pred_paths.size(); i < ped->prev_num_active_ids; ++i)
                marker_array.markers.push_back(toTrajMarker(Eigen::MatrixXd::Zero(0,0),ped->ns, i, 0));

        // std::cout << "marker_array.size() " << marker_array.markers.size() << std::endl;

        return marker_array;
    };

    // visualization -- create marker
    visualization_msgs::Marker toTrajMarker(const Eigen::MatrixXd path, 
        std::string ns, int marker_id, int color_ind)
    {
        visualization_msgs::Marker traj_marker;
        traj_marker.header.frame_id = "/world"; //TODO: should use the frame_id of input msg.
        // traj_marker.header.frame_id = "/map";   //TODO: should use the frame_id of input msg.
        traj_marker.header.stamp = ros::Time::now();
        traj_marker.ns = ns;
        traj_marker.id = marker_id;
        if (color_ind == 0){
            traj_marker.action = visualization_msgs::Marker::DELETE;
        }
        else{
            traj_marker.action = visualization_msgs::Marker::ADD;
        }
        traj_marker.lifetime = ros::Duration(5);
        traj_marker.type = visualization_msgs::Marker::LINE_STRIP;
        traj_marker.pose.orientation.w = 1.0;
        traj_marker.color = color_vec[color_ind];
        traj_marker.scale.x = 0.1;
        // Fill in the traj points
        for (int j = 0; j < path.rows();j++){
          geometry_msgs::Point p;
          p.x = (float) path(j,0);
          p.y = (float) path(j,1);
          // p.z = (float) 0.0;
          traj_marker.points.push_back(p);
        }
        return traj_marker;
    }

};

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "ped_pred");
    PedPredNode ped_predictor;
    ros::spin();
    return 0;
}

