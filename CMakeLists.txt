# Created by Manuel Blum on 2011-05-25.
# Copyright 2013 University of Freiburg.

CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(acl_gp)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  roslib
  # eigen_conversions
  std_msgs
  geometry_msgs
  visualization_msgs
  message_generation
  ford_msgs
)
FIND_PACKAGE(Eigen3 3.0.1 REQUIRED)

# set(CMAKE_CXX_FLAGS_RELEASE "-DCLSQUARE -Wall -O2 -fPIC")
# set(CMAKE_CXX_FLAGS_DEBUG "-DCLSQUARE -Wall -g -fPIC")

# add_message_files(
#   DIRECTORY msg
#   FILES
#   PedestrianPath.msg
#   )

# generate_messages(
#    DEPENDENCIES
#    geometry_msgs
# )

catkin_package(
 INCLUDE_DIRS include
#  LIBRARIES acl_gp
 CATKIN_DEPENDS geometry_msgs roscpp rospy roslib std_msgs visualization_msgs #message_runtime#eigen_conversions
#  DEPENDS system_lib
)



include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})

# ----- Add sources -----
SET(LIBGP_SRC
  src/cov.cc
  src/cov_factory.cc
  src/cov_linear_ard.cc
  src/cov_linear_one.cc
  src/cov_matern3_iso.cc
  src/cov_matern5_iso.cc
  src/cov_noise.cc
  src/cov_rq_iso.cc
  src/cov_periodic_matern3_iso.cc
  src/cov_periodic.cc
  src/cov_se_ard.cc
  src/cov_se_iso.cc
  src/cov_sum.cc
  src/cov_prod.cc
  src/gp.cc
  src/gp_utils.cc
  src/sampleset.cc
  src/rprop.cc
  src/input_dim_filter.cc
  src/cg.cc
)

# ----- Add library target -----
ADD_LIBRARY(acl_gp ${LIBGP_SRC})

# Build exectuables
# ADD_EXECUTABLE(gpdense src/gp_example_dense.cc)
# TARGET_LINK_LIBRARIES(gpdense acl_gp)

# ADD_EXECUTABLE(predictor src/pred_main.cpp)
# TARGET_LINK_LIBRARIES(predictor acl_gp ${catkin_LIBRARIES})

ADD_EXECUTABLE(predictor_node src/ped_predictor.cpp)
TARGET_LINK_LIBRARIES(predictor_node acl_gp ${catkin_LIBRARIES})
add_dependencies(predictor_node ${catkin_EXPORTED_TARGETS})

ADD_EXECUTABLE(debug src/debug.cpp)
TARGET_LINK_LIBRARIES(debug acl_gp ${catkin_LIBRARIES})

