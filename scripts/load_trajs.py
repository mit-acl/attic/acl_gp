#!/usr/bin/env python

import scipy.io as sio
import numpy as np
import pickle
import os
import matplotlib as ml
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def load_file(location, traj_type):
	filename = os.path.dirname(os.path.realpath(__file__)) +\
					 '/../data_folder/trajs_' + traj_type + '_' + location + '_py.mat'
	matlab_obj = sio.loadmat(filename)
	#print trajs_test

	trajs_in = matlab_obj['trajs']
	traj_start_ind = matlab_obj['traj_start_ind']

	#print 'trajs.shape', trajs.shape
	#print 'traj_start_ind.shape', traj_start_ind.shape
	#print 'traj_start_ind', traj_start_ind

	#print trajs_test['data'] 
	trajs = list()
	for i in xrange(traj_start_ind.shape[0] - 1):
		s = traj_start_ind[i,0] - 1
		e = traj_start_ind[i+1,0] - 1# python a[s:e] does not include endpoint e
		#print 's, e, ', s, e
		trajs.append(trajs_in[s:e, :])

	# save to pickle files
	#print trajs_test[0]
	#print trajs_test[1]
	print 'writing to file'
	pickle.dump(trajs, open(file_dir+'/../data_folder/trajs_' + traj_type + \
		'_' + location + '.p', 'wb'))
	return


if __name__ == '__main__':
	file_dir = os.path.dirname(os.path.realpath(__file__))

	# read map from file
	location = 'bldg4'
	# traj_type = 'test'
	# location = 'stata'
	# traj_type = 'test'
	traj_type = 'train'
	load_file(location, traj_type)

