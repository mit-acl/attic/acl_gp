#ifndef __GP_PRED_HPP__
#define __GP_PRED_HPP__



#include "gp.h"
#include "gp_utils.h"
#include "motion_gp.hpp"
#include <assert.h>
#include <math.h>

#include <Eigen/Dense>
#include <string> 
#include <map>

// file handling
#include <iostream>
#include <fstream>

// timing
#include <ctime>

// helper function for splitting a string
template <typename Container>
Container& split(
  Container&                                 result,
  const typename Container::value_type&      s,
  typename Container::value_type::value_type delimiter)
{
  result.clear();
  std::istringstream ss( s );
  while (!ss.eof())
  {
    typename Container::value_type field;
    getline( ss, field, delimiter );
    if (field.empty()) continue;
    result.push_back( field );
  }
  return result;
};

double traj_length(Eigen::MatrixXd traj){
	if (traj.rows() <= 1)
		return 0;
	double dist = 0;
	for (int i = 0; i < traj.rows()-1; ++i)
		dist += sqrt( (traj(i+1,0) - traj(i,0)) * (traj(i+1,0) - traj(i,0)) + 
					  (traj(i+1,1) - traj(i,1)) * (traj(i+1,1) - traj(i,1)));
	return dist;
};

// array of gp motion patterns
class GP_Pred
{
public:
	// variables (may change to private later)
	std::vector <MotionGP*> single_GPs;
	std::map <std::vector<int>, MotionGP*> joint_GPs;
	std::vector <ScBasis*> scBases;
	Eigen::MatrixXi transition_mat;
	double dt;
	int steps;

	// constructors
	GP_Pred(std::string gp_filename, std::string sc_filename, double dt, int steps){
		this->dt = dt;
		this->steps = steps;

		// loading gp file
		std::cout << " loading GP file " << std::endl;
		std::cout << gp_filename << std::endl;
		std::ifstream gp_infile(gp_filename.c_str());

		std::string line;
		while (std::getline(gp_infile, line))
		{
			if (line == "transition_matrix"){
				build_transition_mat(gp_infile);
			}
			else if(line == "single_GP"){
				build_GP(gp_infile);
			}
			else if(line == "joint_GP"){
				build_GP(gp_infile);
			}
		}
		std::cout << transition_mat << std::endl;
		gp_infile.close(); 
		// find set of possible future transitions
		find_vaild_transitions();
		


		// loading sparse coding file
		std::cout << " loading sparse coding basis file " << std::endl;
		std::cout << sc_filename << std::endl;
		std::ifstream sc_infile(sc_filename.c_str());
		while (std::getline(sc_infile, line))
		{
			if (line == "sparse coding basis"){
				build_sc_basis(sc_infile);
			}
		}
		sc_infile.close(); 
		find_num_transitions();
	};

	// destructor
	~GP_Pred(){
		// free single GPs
		for (size_t i = 0; i < this->single_GPs.size(); ++i){
			delete single_GPs[i];
		}

		// free joint GPs
		std::map<std::vector<int>, MotionGP*>::iterator it;
        for (it = joint_GPs.begin(); it != joint_GPs.end();)
        {
            delete it->second;
            joint_GPs.erase(it++);
        }
		
		return;
	};

	// build transition matrix
	void build_transition_mat(std::ifstream &infile){
		std::vector <std::string> fields;
		std::string line;
		std::getline(infile, line);
		split( fields, line, '\t');

		int num_mp = fields.size(), row_ind = 0;
		this->transition_mat = Eigen::MatrixXi::Zero(num_mp, num_mp);

		// empty line for end of section
		while (line != ""){
			split( fields, line, '\t');
			for (size_t i=0; i<fields.size(); ++i){
				this->transition_mat(row_ind, i) = std::atoi(fields[i].c_str());
				//std::cout << fields[i] << " , ";
			}	 	
			// std::cout << std::endl; 

			// std::cout << line << std::endl; 
			std::getline(infile, line);
			row_ind += 1;
		}

	};

	void build_GP(std::ifstream &infile){
		std::vector <std::string> fields;
		std::string line;

		// first line is transition
		std::vector<int> transition;
		std::getline(infile, line);
		split( fields, line, '\t');

		// -1 b/c matlab indexing starts at 1
		assert(fields[0] == "transition");
		for (size_t i = 1; i<fields.size(); ++i)
			transition.push_back(std::atoi(fields[i].c_str())-1);

		// first load data into std::vector
		// hyperparam
		std::vector <double> hyperparam_stdvec;
		std::getline(infile, line);
		split( fields, line, '\t');
		assert(fields.size() == 5 && fields[0] == "hyperparam");
		// for length(hyperparam) == 3, <l, sigma_input, sigma_noise>
		// assume lx = ly
		for (size_t i = 2; i < fields.size(); ++i)
			hyperparam_stdvec.push_back(std::atof(fields[i].c_str()));

		// skip "x, y, v_x, v_y" title string
		std::getline(infile, line);

		// training data
		std::vector <double> x_stdvec, y_stdvec, vely_stdvec, velx_stdvec;
		std::getline(infile, line);
		// empty line for end of section
		while (line != ""){
			split( fields, line, '\t');
			x_stdvec.push_back(std::atof(fields[0].c_str()));
			y_stdvec.push_back(std::atof(fields[1].c_str()));
			velx_stdvec.push_back(std::atof(fields[2].c_str()));
			vely_stdvec.push_back(std::atof(fields[3].c_str()));
			 	
			// std::cout << std::endl; 

			// std::cout << line << std::endl; 
			std::getline(infile, line);
		}

		// construct motion GP
		int l = x_stdvec.size();
		Eigen::VectorXd hyperparam(hyperparam_stdvec.size()), x(l), y(l), velx(l), vely(l);
		stdVec_2_eigVec(hyperparam_stdvec, hyperparam);
		stdVec_2_eigVec(x_stdvec, x);
		stdVec_2_eigVec(y_stdvec, y);
		stdVec_2_eigVec(velx_stdvec, velx);
		stdVec_2_eigVec(vely_stdvec, vely);
		Eigen::MatrixXd pos(x.rows(), 2);
		pos << x, y;

		// debuggin
		// std::cout << "hyperparam size " << hyperparam.rows() << " " 
		// 		<< hyperparam.cols() << "\n"<< hyperparam << std::endl;
		// std::cout << "pos size " << pos.rows() << " " << pos.cols() << "\n"<< pos << std::endl;
		// std::cout << "velx size " << velx.rows() << " " << velx.cols() << "\n"<< velx << std::endl;
		// std::cout << "velx size " << vely.rows() << " " << vely.cols() << "\n"<< vely << std::endl;
		// std::cout << "hyperparam size " << hyperparam.rows() << " " 
		// 		<< hyperparam.cols() << std::endl;
		// std::cout << "pos size " << pos.rows() << " " << pos.cols() << std::endl;
		// std::cout << "velx size " << velx.rows() << " " << velx.cols() << std::endl;
		// std::cout << "velx size " << vely.rows() << " " << vely.cols() << std::endl;


		MotionGP* m_gp = new MotionGP(pos, velx, vely, hyperparam, transition, this->dt, this->steps);

		assert (transition.size() == 1 || transition.size() == 2);

		// single_GPs
		if (transition.size() == 1){
			assert(transition[0] == single_GPs.size());
			single_GPs.push_back(m_gp);
			std::cout << "single_GP "<< transition[0] << std::endl;
		}
		//joint_GPs
		else if (transition.size() == 2){
			// verify that this transition is new (haven't seen this before)
			std::map<std::vector<int>,MotionGP*>::iterator it = joint_GPs.find(transition);
        	assert(it == joint_GPs.end());
            joint_GPs[transition] = m_gp;
            std::cout << "joint_GP " << transition[0] 
            		  << " " << transition[1] << std::endl;
        }
		else{
			assert (false);
		}

		

	}; // void build_GP(std::ifstream &infile)



	void build_sc_basis(std::ifstream &infile){
		std::vector <std::string> fields;
		std::string line;

		// first line is id
		std::getline(infile, line);
		split( fields, line, '\t');
		int id = std::atoi(fields[1].c_str())- 1;
		assert(id == this->scBases.size());

		// second line is header (skip)
		std::getline(infile, line);

		// third line is param
		std::vector <double> param;
		std::getline(infile, line);
		split( fields, line, '\t');
		for (size_t i = 0; i<fields.size(); ++i)
			param.push_back(std::atof(fields[i].c_str()));

		// fourth line is header (skip)
		std::getline(infile, line);

		// data (only using x_cor, y_cor for now)
		std::vector<int> x_cord;
		std::vector<int> y_cord;
		std::getline(infile, line);
		while (line[0] != 'r'){
			split( fields, line, '\t');
			x_cord.push_back(std::atoi(fields[0].c_str()));
			y_cord.push_back(std::atoi(fields[1].c_str()));
			// std::cout << "x_cord, y_cord " << x_cord[x_cord.size()-1] << 
			//            "  " << y_cord[y_cord.size()-1] << std::endl;
			std::getline(infile, line);
		}

		ScBasis* sc_basis = new ScBasis(param, x_cord, y_cord);

		// load boundary info
		int num_x = (int) (param[5]+0.2);
		int num_y = (int) (param[6]+0.2);  //(int) always round down
		// rows_min_bnd 
		while (line[0] != 'c'){
			std::getline(infile, line);
			split( fields, line, '\t');
			sc_basis->rows_min_bnd.push_back(std::atof(fields[0].c_str()));
			sc_basis->rows_max_bnd.push_back(std::atof(fields[1].c_str()));
		}

		// cols_min_bnd
		while (line != ""){
			std::getline(infile, line);
			split( fields, line, '\t');
			sc_basis->cols_min_bnd.push_back(std::atof(fields[0].c_str()));
			sc_basis->cols_max_bnd.push_back(std::atof(fields[1].c_str()));
		}

		// get blank line
		// std::getline(infile, line);

		scBases.push_back(sc_basis);
		std::cout << "sc_basis "<< id << std::endl;

	};


	// convert std::vector<double> to Eigen::VectorXd
	void stdVec_2_eigVec(const std::vector<double> &std_vec, Eigen::VectorXd &eig_vec){
		for (size_t i = 0; i < std_vec.size(); ++i)
			eig_vec(i) = std_vec[i];
	};


	// findBestSingle_GP
	int findBestSingle_GP(Eigen::MatrixXd obs_pos){

		// making sure obs_pos is not too short or too long
		int gp_num = -1;

		double threshold = -0.3;
		gp_num = -5;
		// compute log-likelihood
		int num_gps = this->single_GPs.size();
		std::vector <double> log_likelihood(num_gps, 0.0);
		for (int i = 0; i<num_gps; ++i){
			if (ifTrajInScBasis(obs_pos, i) == true)
				log_likelihood[i] = single_GPs[i]->traj_likelihood_indep(obs_pos);
			else
				log_likelihood[i] = -9999;
		}

		// find the maximum entry
		double max_l = - 1000;
		int max_int;
		for (int i = 0; i < num_gps; ++i)
			if (log_likelihood[i] > max_l){
				max_l = log_likelihood[i];
				max_int = i;
			}

		// for debugging
		// if (max_l < threshold)
		// 	std::cout << "likelihood too low" << std::endl;
		// for (size_t i = 0; i < num_gps; ++i){
		// 	if (log_likelihood[i] > threshold)
		// 	     std::cout << "i: " << i << " l " << log_likelihood[i] << std::endl;
		// }
		// std::cout << "max i: " << max_int << " ------------------" << std::endl;
		for (int i = 0; i < num_gps; ++i){
			     std::cout << log_likelihood[i] << " ";
		}
		std::cout << std::endl;
		// see if max likelihood exceeds a preset threshold
		if (max_l > threshold){
			gp_num = max_int;
			return gp_num;
		} 
		std::cout << "can't do gp prediction because likelihood too low: " << max_l <<std::endl;
		return -1;
	};


	// determine the set of feasible future transitions from the transition matrix
	void find_vaild_transitions(){
		int num_gps = transition_mat.rows();
		for (int i = 0; i < num_gps; ++i){
			// for each single gp
			for (int j = 0; j < num_gps; ++j){
				if (j != i && transition_mat(i,j) > 0)
					single_GPs[i]->next_gp_transitions.push_back(j);
			}
		}
	};

	// void find the number of transitions into and out of each mp
	void find_num_transitions(){
		for (size_t i = 0; i < transition_mat.rows(); ++i){
			scBases[i]->trans_self = transition_mat(i,i);
			scBases[i]->trans_out = transition_mat.row(i).sum() - transition_mat(i,i);
			scBases[i]->trans_in = transition_mat.col(i).sum() - transition_mat(i,i);
		}
		return;
	};

	// find the set of GPs for prediction
	bool find_predGPs(int cur_gp_ind, const std::vector <MotionGP*> prev_gps,
		              std::vector<MotionGP*> & predGPs){

		// add new gps dictated by a valid cur_gp_ind
		if (cur_gp_ind >= 0){
			// c++ vector deep copy 
			std::vector <int> next_gp_inds = single_GPs[cur_gp_ind]->next_gp_transitions;

			// std::cout << "in find_predGPs 1" << std::endl;
			// if cur_gp is a dead-end (doesn't transition into other gps)
			if (next_gp_inds.size() == 0){
				predGPs.push_back(single_GPs[cur_gp_ind]);
			}
			// if cur_gp does exhibit transition into other gps 
			else{
				predGPs.push_back(single_GPs[cur_gp_ind]);
				std::vector <int> key(2,0);
				//std::cout << "find_predGPs key: " << key[0] << " " << key[1] << std:endl;
				key[0] = cur_gp_ind;
				std::map< std::vector<int> , MotionGP*>::iterator it;
				for (size_t i = 0; i < next_gp_inds.size(); ++i){
					key[1] = next_gp_inds[i];
					// std::cout << "find_predGPs key: " << key[0] << " " << key[1] << std::endl;
					it = joint_GPs.find(key);
					assert (it != joint_GPs.end());
					predGPs.push_back(it->second);
				}
			}
		}
		std::cout << "transition from " << cur_gp_ind << " h1 predGPs.size() " 
		<< predGPs.size() << std::endl;

		// add gps from the previous iteration that aren't in the current set
		int num_current_pred = predGPs.size();
		for (size_t i = 0; i < prev_gps.size(); ++i){
			// only add joint gps
			if (prev_gps[i]->transition.size() > 1){
				bool if_Appeared = false;
				for (int j = 0; j < num_current_pred; ++j){
					// repeat
					// if (prev_gps[i]->transition == predGPs[j]->transition){
					// 	if_Appeared = true;
					// 	break;
					// }
					// single gp
					if (prev_gps[i]->transition.back() == predGPs[j]->transition[0]
						|| prev_gps[i]->transition.back() == predGPs[j]->transition.back()){
						if_Appeared = true;
						break;
					}
				}
				if (if_Appeared == false){
					// prev_gps[i]->print_motion_gp("adding gp from prev_gps");
					predGPs.push_back(prev_gps[i]);
				}
			}
		}

		// debugging
		// transition from 
		std::cout << "transition from " << cur_gp_ind << " h2 predGPs.size() " 
		<< predGPs.size() << std::endl;
		for (size_t i = 0; i<predGPs.size(); ++i){
			predGPs[i]->print_motion_gp("in gp_pred, near end");
		}
		return (predGPs.size() > 0);
	};

	// make prediction given current observation
	bool make_vec_of_pred(std::vector <Eigen::MatrixXd> &pred_paths, 
						  std::vector <double> &pred_likelihood, 
						  std::vector <MotionGP*> &prev_gps, 
						  Eigen::MatrixXd obs_pos_in, double v){
		std::clock_t start_time = clock();  // time this function

		// preprocessing
		int gp_num;
		Eigen::MatrixXd obs_pos;
		int num_pts = processInput(obs_pos_in);
		if (num_pts <= 0){
			std::cout << "~~~ preprocess failed ~~~"<<std::endl;
			gp_num = -5;
			obs_pos = obs_pos_in.bottomRows(1);
		}
		else{
			gp_num = findBestSingle_GP(obs_pos_in);
			obs_pos = obs_pos_in.bottomRows(num_pts);
		}

		// true for using gp predictions
		// false for using linear prediction
		bool if_gp_pred;
		assert(obs_pos.rows() > 0);
		// predict with each selected GP
		std::vector<MotionGP*> predGPs;
		std::cout << "gp_num " << gp_num << std::endl;
		std::cout << "prev_gps.size() " << prev_gps.size() << std::endl;

		if (gp_num < 0 && prev_gps.size() == 0){
			if_gp_pred = false;
			prev_gps.clear();
			std::cout << "failed here" << std::endl;
		}
		else {
			if_gp_pred = find_predGPs(gp_num, prev_gps, predGPs);
			prev_gps.clear();
			// std::cout << "in make_vec_of_pred 2" << std::endl;
			// assert (predGPs.size() > 0);
			for (size_t i = 0; i < predGPs.size(); ++i){
				// only add joint GPs

					Eigen::MatrixXd pred_path = predGPs[i]->make_pred(obs_pos.row(obs_pos.rows()-1), v);
					int num_points = ifPredTrajInBasis(pred_path, predGPs[i]->transition);
					if (num_points <= 1)
						continue;
					else{
						prev_gps.push_back(predGPs[i]);          // record a valid GP
						pred_paths.push_back(pred_path.topRows(num_points));
						pred_likelihood.push_back(predGPs[i]->traj_likelihood_indep(obs_pos));
					}
				

			}

			// then try single GP and see if it is sufficiently different from previous predictions
			// if (gp_num>=0){
			// 	// yell("whether should add single basis");
			// 	Eigen::MatrixXd pred_path = single_GPs[gp_num]->make_pred(obs_pos.row(obs_pos.rows()-1), v);
			// 	int num_points = ifPredTrajInBasis(pred_path, single_GPs[gp_num]->transition);
			// 	// check if repetitive prediction
			// 	if (num_points > 1){
			// 	// 	for (size_t i = 0; i < pred_paths.size(); ++i)
			// 	// 	{
			// 	// 		if (ifPathSimilar(pred_path, pred_paths[i]) == true){
			// 	// 			// yell("do not add single basis");
			// 	// 			ifAdd = false;
			// 	// 			break;
			// 	// 		}
			// 	// 	}
			// 	// }
			// 	// if (ifAdd == true){
			// 		// yell("added single basis");
			// 		prev_gps.push_back(single_GPs[gp_num]);
			// 		pred_paths.push_back(pred_path.topRows(num_points));
			// 		pred_likelihood.push_back(single_GPs[gp_num]->traj_likelihood_indep(obs_pos));
			// 	}
			// }

			// still no added predicted paths
			if (pred_paths.size() == 0)
				if_gp_pred = false;
		}

		
		clock_t end_time = clock();
		double elapsed_ms = double (end_time - start_time) / CLOCKS_PER_SEC * 1000;
		for (size_t i = 0; i< prev_gps.size(); i++){
			if (prev_gps[i]->transition.size() == 1)
				std::cout << "chose gp with transition: " << prev_gps[i]->transition[0] << std::endl;
			else
				std::cout << "chose gp with transition: " << prev_gps[i]->transition[0] 
						<< " " << prev_gps[i]->transition[1] << std::endl;
		}
		std::cout << "==== elapsed time (ms): " << elapsed_ms << " if_gp_pred: " << if_gp_pred << 
		" num_gp_path " << pred_paths.size() << std::endl; 
		// assert (pred_paths.size() == pred_likelihood.size());
		// assert (pred_paths.size() == prev_gps.size());
		// for (size_t i = 0; i < pred_paths.size(); ++i)
		// 	assert (pred_paths[i].rows() > 1);
		// std::cout << "in make_vec_of_pred 3" << std::endl;
		return if_gp_pred;
	};
	
	// process input (prediction based on observation in the past few meters)
	int processInput(Eigen::MatrixXd obs_pos){
		double dist = 0, dist_thres_max = 3, dist_thres_min = 1;
		int num_pts = obs_pos.rows();

		for (int i = obs_pos.rows()-1; i > 0; --i)
		{
			dist += sqrt((obs_pos(i,0) - obs_pos(i-1,0)) * (obs_pos(i,0) - obs_pos(i-1,0)) 
				         + (obs_pos(i,1) - obs_pos(i-1,1)) * (obs_pos(i,1) - obs_pos(i-1,1)));
			// set num_pts only once
			if (num_pts == obs_pos.rows() && dist > dist_thres_max)
				num_pts = (obs_pos.rows()-1) - i + 2;
		}

		// observed traj is too short
		if (dist < dist_thres_min || obs_pos.rows() < 3){
			return -5;
		}

		return num_pts;
	};

	// test whether a point belongs to the active region of a sparse coding basis
	bool ifPointInScBasis(double x, double y, int sc_number){
		ScBasis* sc_basis = scBases[sc_number];

		// more conservative, without max pooling
		// in bound 
		if (x < sc_basis->min_x_tick || x > sc_basis->max_x_tick || 
			   y < sc_basis->min_y_tick || y > sc_basis->max_y_tick )
			return false;

		// whether grid location is active
		int x_ind = (int) (x-sc_basis->min_x_tick) / sc_basis->width;
		int y_ind = (int) (y-sc_basis->min_y_tick) / sc_basis->width;
		if (x < sc_basis->rows_min_bnd[y_ind] - 0.5 || x > sc_basis->rows_max_bnd[y_ind] + 0.5 || 
			y < sc_basis->cols_min_bnd[x_ind] - 0.5 || y > sc_basis->cols_max_bnd[x_ind] + 0.5){
			return false;
		}

		return (bool) sc_basis->activeReg(x_ind, y_ind);

		// with max pooling
		// in bound 
		// if (x < sc_basis->min_x_tick - sc_basis->width || x > sc_basis->max_x_tick + sc_basis->width || 
		// 	y < sc_basis->min_y_tick - sc_basis->width || y > sc_basis->max_y_tick + sc_basis->width )
		// 	return false;

		// // whether grid location is active
		// int x_ind = (int) (x-sc_basis->min_x_tick) / sc_basis->width;
		// int y_ind = (int) (y-sc_basis->min_y_tick) / sc_basis->width;

		// // max pooling
		// for (int i = -1; i<=1; ++i)
		// 	for (int j = -1; j<=1; ++j)
		// 	{
		// 		int ind_shift_x = x_ind + i;
		// 		int ind_shift_y = y_ind + j;
		// 		if (ind_shift_x < 0 || ind_shift_x >= sc_basis->num_x ||
		// 			ind_shift_y < 0 || ind_shift_y >= sc_basis->num_y)
		// 			continue;
		// 		if (sc_basis->activeReg(ind_shift_x, ind_shift_y) == 1)
		// 			return true;
		// 	}
		// return false;

	};

	// test whether a point belongs to the active region of a sparse coding basis 
	// and its children (gps to transition into)
	bool ifPointInChildrenScBasis(double x, double y, int sc_number){

		if (ifPointInScBasis(x,y,sc_number) == true)
			return true;
		else
		{
			for (int i = 0; i < single_GPs[sc_number]->next_gp_transitions.size(); ++i)
			{
				if (ifPointInScBasis(x,y, single_GPs[sc_number]->next_gp_transitions[i]) == true)
					return true;
			}
		}
		return false;
	};


	// test if an observed trajectory is coming from a sc_basis
	bool ifTrajInScBasis(Eigen::MatrixXd &obs_pos, int sc_number){
		// find whether the past several points are inside the active region
		double seg_length = 1; // just test the past several points

		ScBasis* sc_basis = scBases[sc_number];

		// if sc_basis itself had enough observations
		if ((sc_basis->trans_in + sc_basis->trans_out + sc_basis->trans_self) < 0)
			return false;

		double dist = 0;

		for (size_t i = obs_pos.rows()-1; i > 0; --i)
		{
			dist += sqrt((obs_pos(i,0) - obs_pos(i-1,0)) * (obs_pos(i,0) - obs_pos(i-1,0)) 
				         + (obs_pos(i,1) - obs_pos(i-1,1)) * (obs_pos(i,1) - obs_pos(i-1,1)));
			// if any point is not inside the active region
			if (ifPointInScBasis(obs_pos(i,0), obs_pos(i,1), sc_number) == false)
				return false;
			if (dist > seg_length)
				break;
		}
		return true;
	};

	// number of points until a prediction exits a valid sparse code basis
	int ifPredTrajInBasis(Eigen::MatrixXd traj, std::vector <int> & sc_inds){
		int num_points;
		assert (sc_inds.size() == 1 || sc_inds.size() == 2);
		double cutting_corner_thres = 1.1; // partial segement of the predicted path can be outside
										   // of the active region, but needs to be below this length

		// single sc_basis
		if (sc_inds.size() == 1){
			// // more conservative version
			// num_points = traj.rows();
			// for (size_t i = 1; i < traj.rows(); ++i){
			// 	if (ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[0]) == false){
			// 		num_points = i-1;
			// 		break;
			// 	}
			// }

			// // more aggressive version
			// num_points = 0;
			// for (size_t i = traj.rows()-1; i > 0; i--){
			// 	if (ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[0]) == true){
			// 		num_points = i;
			// 		break;
			// 	}
			// }
			
			// making sure that there is not cutting corner
			num_points = 1;
			for (size_t i = 1; i < traj.rows(); ++i){
				if (ifPointInChildrenScBasis(traj(i,0), traj(i,1), sc_inds[0]) == true){
					num_points = i;
				}else{
					if (traj_length(traj.middleRows(num_points, (int)i - num_points)) > cutting_corner_thres)
						break;
				}
			}
			// always return non-negative ind for single GPs
			return std::max(5,num_points);
				
			

			std::cout << "single sc basis, num_points: " << num_points<< std::endl;
		} // if statement for transition gp
		// transition between two basis
		else{
			// // more conservative version
			// double seg_frac = 1.0/6.0; 
			// int num_points_2 = 0;
			// for (size_t i = 1; i < traj.rows(); ++i){
			// 	// belong to second basis
			// 	if (ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[1]) == true)
			// 		num_points_2 += 1;
			// 	else if (ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[0]) == true)
			// 		num_points_2 = 0;
			// 	else{
			// 		num_points = i - 1;
			// 		// std::cout << "joint sc  basis, num_points: " << num_points<< std::endl;
			// 		break;
			// 	}
			// }
			// more aggressive version
			double seg_frac = 1.0/6.0; 
			int num_points_2 = 0;
			std::vector <bool> ifInFirstBasis(traj.rows(), false), ifInSecondBasis(traj.rows(), false);
			for (size_t i = 0; i < traj.rows(); ++i){
				ifInFirstBasis[i] = ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[0]);
				ifInSecondBasis[i] = ifPointInScBasis(traj(i,0), traj(i,1), sc_inds[1]);
			}
			//debugging
			std::cout << "ifInFirstBasis";
			for (size_t i = 0; i < traj.rows(); ++i)
				std::cout << (int) ifInFirstBasis[i] << " " ;
			std::cout << std::endl;

			std::cout << "ifInSecondBasis";
			for (size_t i = 0; i < traj.rows(); ++i)
				std::cout << (int) ifInSecondBasis[i] << " " ;
			std::cout << std::endl;

			// first find num_points_1
			int num_points_1 = 0;
			for (size_t i = 0; i < traj.rows(); ++i){
				if (ifInFirstBasis[i] == true){
					num_points_1 = i;
				}else{
					if (traj_length(traj.middleRows(num_points_1, i-num_points_1)) 
						> cutting_corner_thres)
						break;
				}
			}

			// then num_points_2_min_ind
			int num_points_2_min_ind = 0;
			for (size_t i = 0; i < traj.rows(); ++i){
				if (ifInSecondBasis[i] == true){
					num_points_2_min_ind = i;
					break;
				}else{
					if (i>num_points_1 && 
						traj_length(traj.middleRows(num_points_1, i-num_points_1)) 
						> cutting_corner_thres)
						break;
				}
			}

			// lastly find num_points_2_max_ind
			int num_points_2_max_ind = num_points_2_min_ind;
			for (int i = num_points_2_min_ind; i < traj.rows(); ++i){
				if (ifInSecondBasis[i] == true){
					num_points_2_max_ind = i;
				}else{
					if (traj_length(traj.middleRows(num_points_2_max_ind, i-num_points_2_max_ind)) 
						> cutting_corner_thres)
						break;
				}
			}

			std::cout << "joint sc, num_points_1, num_points2_min, num_points2_max " << 
					num_points_1 << " " << num_points_2_min_ind << " " << num_points_2_max_ind << std::endl;

			num_points_2 = num_points_2_max_ind - num_points_2_min_ind + 1;
			num_points = num_points_2_max_ind;
		
			// require part belonging to second basis to be  
			// at least of a certain length
			std::cout << "joint sc, transition" << sc_inds[0] << " "<< sc_inds[1] << std::endl;
			std::cout << "joint sc  basis, num_points2, num_points , traj.rows() " << num_points_2 
					<< " " << num_points << " " << traj.rows() << std::endl;
			if (num_points_2 / ((double) traj.rows()) < seg_frac)
				return 0;
			

		} // else statement for transition gp
		double traj_length_thres = 3;
		double pred_length_ratio = 0.5;
		if (traj_length(traj.topRows(num_points)) < traj_length_thres 
			&& ((double) num_points / traj.rows()) < pred_length_ratio ){
			if (sc_inds.size() == 1)
				std::cout << "single gp prediction not long enough" << std::endl;
			return 0;
		}
		else{
			if (sc_inds.size() == 1)
				std::cout << "single gp prediction has " << num_points << " points" << std::endl;
			std::cout << "traj_length " << traj_length(traj.topRows(num_points)) << std::endl;
			std::cout << "traj_length_ratio " << ((double) num_points / traj.rows()) << std::endl;
			return num_points;
		}
	};

	// debugging print to screen
	void yell(std::string str_in){
		std::cout << str_in << std::endl;
	};
};



#endif