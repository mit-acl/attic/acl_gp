%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For developling GP prediction in C++
%
% Steven Chen, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;

addpath plotting
rng(1);
x = (0:0.1:1)';
y = linspace(-1.5, -2, length(x))';

noise = 0.2;
x = x + noise * rand(size(x));
y = y + noise * rand(size(y));

% build model
param = build_linear_model(x,y);

% make prediction
dt = 0.5;
v = 0.5;
steps = 10;
pred  = linear_model_predict(param, x, y, v, dt, steps);
plot_linear_model(param, x, y, pred);

% simple numerical test
x = [1.1, 1.2, 1.4]';
y = [0.1, 0.15, 0.21]';
param = build_linear_model(x,y);
dt = 0.5;
v = 0.5;
steps = 3;
pred  = linear_model_predict(param, x, y, v, dt, steps);
fprintf(sprintf('param: %f, %f\n',param(1), param(2)));
fprintf(sprintf('pred_x: '));
for i = 1:steps+1
    fprintf(sprintf('\t %f', pred(i,1)));
end
fprintf('\n');

fprintf(sprintf('pred_y: '));
for i = 1:steps+1
    fprintf(sprintf('\t %f', pred(i,2)));
end
fprintf('\n');
plot_linear_model(param, x, y, pred);
