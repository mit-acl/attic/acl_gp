#include <ros/ros.h>
#include <iostream>
#include "linear_pred.hpp"
#include "motion_gp.hpp"
#include "ped_predictor.hpp"
// #include "gp.h"

#include <eigen_conversions/eigen_msg.h>
// #include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>
#include <acl_gp/PedestrianPath.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/ColorRGBA.h>
#include <string> 

class LinearPredNode{
public:
	ros::NodeHandle nh_p_;
	ros::Subscriber sub_input_;
	ros::Publisher pub_output_;
	ros::Publisher pub_markers_;

	double dt_;
	int steps_;


	LinearPredNode()
	{
		nh_p_ = ros::NodeHandle("~");
		setDefaultParameters();
		readParameters();
		sub_input_ = nh_p_.subscribe("input",1,&LinearPredNode::cbInput, this);
		pub_output_ = nh_p_.advertise<acl_gp::PedestrianPath>("output",1,true); //true for latching
		
		pub_markers_ = nh_p_.advertise<visualization_msgs::MarkerArray>("markers",1,true); //true for latching
		ROS_INFO_STREAM("[linear_pred]: Initialized. " << " dt = " << dt_ << " steps = " << steps_);
	}
	~LinearPredNode(){}

	void setDefaultParameters()
	{
		if (!ros::param::has("~dt")) { ros::param::set("~dt",0.5);}
		if (!ros::param::has("~steps")) { ros::param::set("~steps",3);}
	}

	void readParameters()
	{
		ros::param::getCached("~dt",dt_);
		ros::param::getCached("~steps",steps_);
	}

	void cbInput(const acl_gp::PedestrianPath& input_msg)
	{
		// TODO read dt and steps as parameters
		ROS_INFO_STREAM("[linear_pred]: input " << input_msg.path);
		readParameters();
		// x << 1.1, 1.2, 1.4;
		// y << 0.1, 0.15, 0.21;

		LinearPred linear_predictor = LinearPred(dt_, steps_);
		
		int num_of_points = input_msg.path.points.size();
		// === Construct pos Matrix === //
		Eigen::VectorXd x(num_of_points),y(num_of_points);
		for (int i = 0; i < num_of_points; i++){
			x(i) = input_msg.path.points[i].x;
			y(i) = input_msg.path.points[i].y;
		}
		Eigen::MatrixXd pos(x.rows(),2);
		pos << x, y;

		// === Make Prediction === //
		Eigen::MatrixXd predMatrix = linear_predictor.make_pred(pos, input_msg.v);
		
		acl_gp::PedestrianPath output_msg;
		output_msg.v = input_msg.v;
		output_msg.id = input_msg.id;
		//geometry_msgs::Polygon output_msg;
		for (int i = 0; i < predMatrix.rows(); i++){
			geometry_msgs::Point32 point;
			point.x = predMatrix.row(i)[0];
			point.y = predMatrix.row(i)[1];
			output_msg.path.points.push_back(point);
		}
		pub_output_.publish(output_msg);
		ROS_INFO_STREAM("[linear_pred]: output " << output_msg);
		
		// === Publish Markers === //
		visualization_msgs::MarkerArray marker_array;
		std_msgs::ColorRGBA color_red;
		color_red.r = 1.0;
		color_red.a = 1.0;
		std_msgs::ColorRGBA color_green;
		color_green.g = 1.0;
		color_green.a = 1.0;
		marker_array.markers.push_back(toTrajMarker(input_msg,"linear_pred",0,color_green));
		marker_array.markers.push_back(toTrajMarker(output_msg,"linear_pred",1,color_red));
		pub_markers_.publish(marker_array);
	}

	visualization_msgs::Marker toTrajMarker(const acl_gp::PedestrianPath ped_path, std::string ns, int id, std_msgs::ColorRGBA color)
	{
		visualization_msgs::Marker traj_marker;
		traj_marker.header.frame_id = "/world";
		traj_marker.header.stamp = ros::Time::now();
		traj_marker.ns = ns + to_string(ped_path.id);
		traj_marker.id = id;
		traj_marker.action = visualization_msgs::Marker::ADD;
		traj_marker.lifetime = ros::Duration(0);
		traj_marker.type = visualization_msgs::Marker::LINE_STRIP;
		traj_marker.pose.orientation.w = 1.0;
		traj_marker.color = color;
		traj_marker.scale.x = 0.045;
		traj_marker.scale.y = 0.03;
		traj_marker.scale.z = 0.03;
		// Fill in the traj points
		for (int j = 0; j < ped_path.path.points.size();j++){
		  geometry_msgs::Point p;
		  p.x = (float) ped_path.path.points[j].x;
		  p.y = (float) ped_path.path.points[j].y;
		  // p.z = (float) 0.0;
		  traj_marker.points.push_back(p);
		}
		return traj_marker;
	}
};


int main(int argc, char** argv)
{
	ros::init(argc, argv, "linear_pred");
	LinearPredNode linear_pred_node;
	ros::spin();
	return 0;
}