function pred = linear_model_predict(param, x, y, v, dt, steps)

num_pts = min(10, length(x));
if num_pts == 10
    x = x(end-9:end);
    y = y(end-9:end);
end

% find out heading direction
dir_mag = [x(end) - x(1), y(end) - y(1)];
dir_sign = sign(dir_mag);
if abs(dir_mag(1)) > abs(dir_mag(2))
    dir_sign(2) = sign( dir_mag(1) * param(1));
else
    dir_sign(1) = sign( dir_mag(2) * param(1));
end

% forward propagate from (x(end), y(end));
pred = zeros(steps+1, 2);

ds = v * dt;
dx = ds * sqrt(1/(1 + param(1)^2));
dy = ds * abs(param(1)) * sqrt(1/(1 + param(1)^2));


pred(:,1) = x(end) + dir_sign(1) * dx * (0:steps)';
pred(:,2) = y(end) + dir_sign(2) * dy * (0:steps)';


end