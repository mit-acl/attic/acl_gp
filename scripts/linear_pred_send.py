#!/usr/bin/env python
import rospy

from acl_gp.msg import PedestrianPath
from geometry_msgs.msg import Point32

if __name__ == '__main__':
    rospy.init_node('linear_pred_sender',anonymous=False)
    pub = rospy.Publisher('linear_pred/input',PedestrianPath, queue_size=1, latch=True)
    # === Set parameters === #
    rospy.set_param('linear_pred/dt',0.5)
    rospy.set_param('linear_pred/steps',3)

    # predestrian 1
    id_1 = 1
    vel_1 = 0.5
    ped_path_msg = PedestrianPath()
    ped_path_msg.id = id_1
    ped_path_msg.v = vel_1

    ped_path_msg.path.points.append(Point32(x=1.1, y=0.1))
    ped_path_msg.path.points.append(Point32(x=1.2, y=0.15))
    ped_path_msg.path.points.append(Point32(x=1.4, y=0.21))
    pub.publish(ped_path_msg)
    rospy.sleep(0.5) #To ensure it gets published before the node dies.


















