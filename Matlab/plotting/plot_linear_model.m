function plot_linear_model(param, x, y, pred)
figure

plot(x, y, 'ro')
hold on
x_tmp = min(x) - 0.5: 0.1: max(x) + 0.5;
y_tmp = [x_tmp; ones(size(x_tmp))]' * param;
plot(x_tmp, y_tmp, 'b');

title('weighted linear interpolation');

plot (x(end), y(end), 'k*');
plot (pred(:, 1), pred(:, 2), 'g-o');

legend('data', 'interp', 'end point', 'pred');
end