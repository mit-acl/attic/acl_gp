#ifndef __M_GP_HPP__
#define __M_GP_HPP__



#include "gp.h"
#include "gp_utils.h"
#include <assert.h>
#include <math.h>

#include <Eigen/Dense>




class VelocityGP
{
public:
	// constructors
	VelocityGP(Eigen::MatrixXd pos_train, Eigen::VectorXd vel_train, 
		Eigen::VectorXd hyperparam){

		// initialize Gaussian process for 2-D input using the squared exponential 
  		// covariance function with additive white noise.
		gp = new libgp::GaussianProcess(2, "CovSum ( CovSEiso, CovNoise)");

		// initialize hyper parameter vector
		// for length(hyperparam) == 3, <l, sigma_input, sigma_noise>
		assert( hyperparam.rows() == gp->covf().get_param_dim() );
  		// set parameters of covariance function
  		gp->covf().set_loghyper( hyperparam.array().log() );

  		// add training patterns
  		for (size_t i=0; i<vel_train.rows(); ++i){
  			double input[] = {pos_train(i,0), pos_train(i,1)};
  			double output = vel_train(i);
  			gp->add_pattern(input, output);
  		}

  		// std::cout << "training inputs \n" << pos_train << std::endl;
  		// std::cout << "training outputs \n" << vel_train << std::endl;
	};

	// destructor
	~VelocityGP(){
		delete gp;
	};

	// making prediction
	// for debugging only, should not be invoked
	Eigen::VectorXd make_pred (Eigen::MatrixXd pos_query){
		Eigen::VectorXd pred = Eigen::VectorXd::Zero(pos_query.rows());
		double input[2];
		for (size_t i = 0; i < pos_query.rows(); ++i)
		{
			input[0] = pos_query(i,0);
			input[1] = pos_query(i,1);
			pred(i) = gp->f(input);
		}
		// std::cout << "query\n" << pos_query << std::endl;
		// std::cout << "predicted values\n" << pred << std::endl;
		return pred;
	};



	libgp::GaussianProcess *gp;

};


class MotionGP{
public:
	std::vector<int> transition;  // single entry (gp id) for a single gp
								  // two entries (from gp_id to gp_id) for a joint jp
	std::vector<int> next_gp_transitions; // empty for a single gp
										  // set of possible next gps for a joint jp
	// constructor
	MotionGP(Eigen::MatrixXd pos_train, Eigen::VectorXd vel_train_x, 
		Eigen::VectorXd vel_train_y, Eigen::VectorXd hyperparam, 
		std::vector<int> transition_in, double dt, int steps){

		// prediction parameters
		this->dt = dt;
		this->steps = size_t(steps);

		gp_x = new VelocityGP(pos_train, vel_train_x, hyperparam);
		gp_y = new VelocityGP(pos_train, vel_train_y, hyperparam);
		transition = transition_in;
		std::cout << "transition.size()" << transition.size() << ";" ;
		if (transition.size() == 1)
			std::cout << transition[0] << std::endl;
		else
			std::cout << transition[0] << " " << transition[1] << std::endl;
	};

	// destructor
	~MotionGP(){
		delete gp_x;
		delete gp_y;
	};

	// make prediction
	Eigen::MatrixXd make_pred(Eigen::Vector2d cur_pos, double v){
		// initialize
		Eigen::MatrixXd pred = Eigen::MatrixXd::Zero(steps+1,2);
		double cur_pos_double[] = { cur_pos(0), cur_pos(1) };
		pred(0,0) = cur_pos_double[0];
		pred(0,1) = cur_pos_double[1];

		for (int i = 1; i < steps+1; i++)
		{
			double vel_x = gp_x->gp->f(cur_pos_double);
			double vel_y = gp_y->gp->f(cur_pos_double);
			double vel_sq = sqrt(vel_x * vel_x + vel_y * vel_y);
			// scaling 
			vel_x /= vel_sq;
			vel_y /= vel_sq;
			cur_pos_double[0] += vel_x * dt * v;
			cur_pos_double[1] += vel_y * dt * v;

			pred(i,0) = cur_pos_double[0];
			pred(i,1) = cur_pos_double[1];
		}
		// debugging
		// std::cout << "distance per prediction" << std::endl;
		// for (size_t i = 1; i < steps+1; i++){
		// 	double x_diff = pred(i,0)-pred(i-1,0); 
		// 	double y_diff = pred(i,1)-pred(i-1,1); 
		// 	std::cout << sqrt(x_diff * x_diff + y_diff * y_diff) << std::endl;
		// }

		return pred;
	}
	
	// compute the log_likelihood
	double traj_likelihood_indep(Eigen::MatrixXd obs_pos){

		double log_likelihood = 0;
		double mu_x, mu_y, var_x, var_y, x_diff, y_diff, lx, ly, v, vx, vy;
		double cur_pos_double[2];
		for (int i = 0; i < obs_pos.rows()-1; ++i){
			// predicted velocities
			cur_pos_double[0] = obs_pos(i,0);
			cur_pos_double[1] = obs_pos(i,1);
			mu_x = gp_x->gp->f(cur_pos_double);
			var_x = gp_x->gp->var(cur_pos_double);
			mu_y = gp_y->gp->f(cur_pos_double);
			var_y = gp_y->gp->var(cur_pos_double);

			// observed velocities
			vx = obs_pos(i+1,0) - obs_pos(i,0);
			vy = obs_pos(i+1,1) - obs_pos(i,1);
			v = sqrt(vx * vx + vy * vy);
			vx /= v;
			vy /= v;

			// compute log likelihood
			x_diff = vx - mu_x;
			y_diff = vy - mu_y;
			lx = -0.5 * log(var_x) - 0.5 * x_diff * x_diff / var_x;
			ly = -0.5 * log(var_y) - 0.5 * y_diff * y_diff / var_y;

			log_likelihood += (lx + ly);
		}

		// normalize based on trajectory's length
		return log_likelihood / (obs_pos.rows() - 1);
	}; // traj_likelihood_indep()

	// for debugging
	void print_motion_gp(std::string input_msg ){
		std::cout << "++++" << input_msg << std::endl;
		std::cout << "print_motion_gp(), trans.: ";
		for (size_t i = 0; i < transition.size(); ++i)
			std::cout << transition[i] << " ";
		std::cout << "  future trans.: ";
		for (size_t i = 0; i < next_gp_transitions.size(); ++i)
			std::cout << next_gp_transitions[i] << " ";
		std::cout << std::endl;
	};


private:
	VelocityGP* gp_x;
	VelocityGP* gp_y;
	double dt;
	size_t steps;

};


// sparse coding basis
class ScBasis{
public:
	double min_x_tick;		// min x grid
	double max_x_tick;		// max x grid
	double min_y_tick;		// min y grid
	double max_y_tick;		// max y grid
	double width;			// side length of grid
	int num_x;				// number of active x grid cords
	int num_y;				// number of active y grid cords
	int trans_self;			// number of trajectories exhibiting this mp only
	int trans_out;			// number of transitions to other mps
	int trans_in;			// number of transitions into this mp

	
	std::vector <double> rows_min_bnd;
	std::vector <double> rows_max_bnd;
	std::vector <double> cols_min_bnd;
	std::vector <double> cols_max_bnd;

	Eigen::MatrixXi activeReg; 

	//constructor
	ScBasis(const std::vector<double> & param, const std::vector<int> & x_cord, 
			const std::vector<int> & y_cord){
		// copy params
		// std::cout << "param.size() " << param.size() << std::endl;
		assert (param.size() == 7);
		min_x_tick = param[0];
		max_x_tick = param[1];
		min_y_tick = param[2];
		max_y_tick = param[3];
		width = param[4];
		num_x = (int) (param[5]+0.2);
		num_y = (int) (param[6]+0.2);  //(int) always round down


		// build matrix
		// std::cout << "size: " << num_x << " " << num_y << std::endl;
		activeReg = Eigen::MatrixXi::Zero(num_x, num_y);
		for (size_t i = 0; i < x_cord.size(); i++){
			// std::cout << "ind: " << x_cord[i] << " " <<  y_cord[i] << std::endl;
			activeReg(x_cord[i], y_cord[i]) = 1;
		}

	};

	//destructor
	~ScBasis();

};

#endif



















