function param = build_linear_model(x, y)

num_pts = min(10, length(x));
if num_pts == 10
    x = x(end-9:end);
    y = y(end-9:end);
end

W = eye(num_pts);
W(end, end) = 10;

H = [x, ones(num_pts,1)];
% size(H)
% size(W)
% size(H' *W * H)
% size(H' * W * y)
param = (H' *W * H) \ ( H' * W * y);

end