#ifndef __Pedestrian_HPP__
#define __Pedestrian_HPP__

#include "gp.h"
#include "gp_utils.h"
#include <assert.h>
#include <math.h>
#include <algorithm>
#include <string>
#include "linear_pred.hpp"
#include "gp_pred.hpp"

#include <Eigen/Dense>
#include <stdlib.h>
#include <ctime>

#define MAX_GAP_TIME 10.0
#define GP_TYPE1 1
#define GP_TYPE2 2
#define GP_TYPE3 3
#define LINEAR_TYPE 4
#define DEBUG_TYPE 5
#define MAX_PRED_PATHS 10


template <typename T>
std::string to_string(T value)
{
	std::ostringstream os ;
	os << value ;
	return os.str() ;
}

// inefficient for large vectors
std::vector<size_t> sort_order(std::vector<double> vec_in){
	assert (vec_in.size() > 0);
	std::vector<double> vec_sort = vec_in;
	// sort in descending order
	std::sort(vec_sort.begin(), vec_sort.end(), std::greater<double>());
	// match in the sorted vector
	std::vector<size_t> index(vec_in.size(), 0);
	for (size_t i = 0; i < vec_in.size(); i++)
		for (size_t j = 0; j < vec_in.size(); j++)
			if (fabs(vec_in[i] - vec_sort[j]) < 1e-4){
				index[i] = j;
				break;
			}
	return index;
}

// test if path 1 is similar to path 2
bool ifPathSimilar(const Eigen::MatrixXd &path1, const Eigen::MatrixXd &path2){
	size_t num_pts1 = path1.rows();
	size_t num_pts2 = path2.rows();
	assert (num_pts1 > 1 && num_pts2 > 1);
	size_t ind_2;
	// // require any two points to be no more than dist_thres apart
	// double dist_thres = 1.5;
	// for (size_t i= 0; i<num_pts1; ++i){
	// 	// in case num_pts1 > num_pts2
	// 	if (i >= num_pts2 - 1)
	// 		ind_2 = num_pts2 - 1;
	// 	else
	// 		ind_2 = i;
	// 	if (sqrt ((path1(i,0) - path2(ind_2,0)) * (path1(i,0) - path2(ind_2,0)) + 
	// 		(path1(i,1) - path2(ind_2,1)) * (path1(i,1) - path2(ind_2,1))) > dist_thres)
	// 	{
	// 		return false;
	// 	}
	// }

	// require any two points to be no more than 30 deg apart
	double l_per_step =  sqrt ((path1(1,0) - path1(0,0)) * (path1(1,0) - path1(0,0)) + 
	 		(path1(1,1) - path1(0,1)) * (path1(1,1) - path1(0,1)));
	double sin_10 = sin(10*PI/180);
	double dist_thres = 0.5;
	for (size_t i= 0; i<num_pts1; ++i){
		// in case num_pts1 > num_pts2
		if (i >= num_pts2 - 1)
			ind_2 = num_pts2 - 1;
		else
			ind_2 = i;
		double dist = sqrt ((path1(i,0) - path2(ind_2,0)) * (path1(i,0) - path2(ind_2,0)) + 
							(path1(i,1) - path2(ind_2,1)) * (path1(i,1) - path2(ind_2,1)));
		// std::cout << "i " << i << " dist " << dist << 
		// " thres " << std::max(2.0*((double)i)*l_per_step*sin_10, dist_thres) << 
		// " l_per_step " << l_per_step << std::endl;
		if ( dist > std::max(2.0*((double)i)*l_per_step*sin_10, dist_thres)) 
		{
			return false;
		}
	}

	return true;
}


class Pedestrian{
public:

	int id;
	std::string ns;
	int prev_num_active_ids;

	// time_t last_update_time;
	double last_update_time;
	Eigen::MatrixXd observed_path;
	
	// For prediction
	LinearPred* linear_predictor;
	// for debugging now
	// LinearPred* gp_predictor; 
	GP_Pred* gp_predictor;

	// Prediction Variables
	// Eigen::MatrixXd pred_paths;

	std::vector<Eigen::MatrixXd> pred_paths;
	std::vector <int> pred_type; 
	std::vector <double> pred_likelihood;

	// gps used for prediction in the previous iteration
	std::vector <MotionGP*> prev_gps;

	double dt_;
	int steps_;
	


	// constructors
	Pedestrian(double dt_, int steps_, int id, std::string ns, GP_Pred* gp_pred, double current_time){
		this->id = id;
		this->ns = ns;
		this->dt_ = dt_;
		this->steps_ = steps_;
		this->prev_num_active_ids = 0;
		// this->last_update_time = time(NULL); // last update time 
		this->last_update_time = current_time;
		
		linear_predictor = new LinearPred(dt_, steps_);
		gp_predictor = gp_pred;
		// for debugging
		// gp_predictor = new LinearPred(dt_, steps_);     
		// pred_paths = Eigen::MatrixXd(2*MAX_PRED_PATHS, steps_+1); // [x,y]
	};

	// destructor
	~Pedestrian(){
		delete linear_predictor;
	};

	// make pred
	void make_pred(const Eigen::MatrixXd obs_pos, double v, double current_time){
		std::cout << "pedestrian make_pred(); ped id: " << id << std::endl;
		// last_update_time = time(NULL);	// last update time 
		last_update_time = current_time;

		// book keeping, need to delete these when making markers the next time
		prev_num_active_ids = pred_type.size();
		pred_type.clear();
		pred_paths.clear();
		pred_likelihood.clear();
		// to do: implement GP prediction
		// tmp: place holder for GP prediction, make a number of random predictions 

		// // debugging with linear predictor simulating gp prediction
		// int num_gp_pred = 1 + rand() % 3;
		// for (size_t i = 0; i<num_gp_pred; ++i)
		// {
		// 	Eigen::MatrixXd pred_path = gp_predictor->make_pred(obs_pos, v);
		// 	pred_paths.push_back(pred_path.array() + 0.1*(i+1)); 
		// 	// pred_paths.col(2*i) = pred_paths.col(2*i).array() + 0.1 * 2;
		// 	pred_type.push_back(GP_TYPE);
		// }
		// if ( rand() % 2 == 0)
		// {
		// 	pred_paths.push_back(linear_predictor->make_pred(obs_pos, v));
		// 	pred_type.push_back(LINEAR_TYPE);
		// }
		// if ( true) //rand() % 2 == 0)
		// {
		// 	Eigen::MatrixXd partial_path = linear_predictor->get_observed_partial_path(obs_pos, v);
		// 	pred_paths.push_back(partial_path.array() + 0.05);
		// 	pred_type.push_back(DEBUG_TYPE);
		// }

		std::vector<Eigen::MatrixXd> pred_paths_tmp;
		bool ifGP_predict = gp_predictor->make_vec_of_pred(pred_paths_tmp, pred_likelihood, 
			  prev_gps, obs_pos, v);
		// debugging (print predicted path)
		// for (size_t i = 0; i<pred_paths.size(); ++i)
		// {
		// 	std::cout << "-----------" << std::endl;
		// 	std::cout << pred_paths[i] << std::endl;
			
		// }
		// std::cout << "ifGP_predict: " << ifGP_predict << std::endl;
		
		if (ifGP_predict == true){
			// std::cout << "pred_paths.size(): " << pred_paths.size() << std::endl;
			std::vector<size_t> valid_inds = prune_predictions(pred_likelihood, pred_paths_tmp);
			for (size_t i = 0; i < valid_inds.size(); ++i){
				pred_paths.push_back(pred_paths_tmp[valid_inds[i]]);
				pred_type.push_back(orderInd2type(i));
				// pred_type.push_back(GP_TYPE1);
			}
				
		}else{
			pred_paths.push_back(linear_predictor->make_pred(obs_pos, v));
			pred_type.push_back(LINEAR_TYPE);
		}
	};

	// prune low likelihood and repetitive predictions
	// returns valid index in decreasing order
	std::vector <size_t> prune_predictions(std::vector<double> pred_likelihood, 
		                                   const std::vector<Eigen::MatrixXd>& pred_paths){

		// prune low likelihood paths
		double likelihood_thres = -0.3;

		// prune repeated paths
		std::vector <size_t> valid_inds;
		std::vector <size_t> likelihood_order = sort_order(pred_likelihood);
		std::vector <size_t> sort_order(likelihood_order.size() , 0);
		for (size_t i=0; i < likelihood_order.size(); ++i){
			sort_order[likelihood_order[i]] = i;
		}


		std::vector <bool> if_prune(likelihood_order.size(), false);
		for (size_t i=0; i < sort_order.size(); ++i){
			size_t ind = sort_order[i];
			if (pred_likelihood[i] > likelihood_thres && if_prune[i] == false){
				valid_inds.push_back(ind);
				// prune trajectories with lower likelihood that are similar to 
				// the current best trajectory
				for (size_t j=i+1; j < sort_order.size(); ++j){
					if (ifPathSimilar(pred_paths[sort_order[j]], 
						              pred_paths[ind]) == true){
						if_prune[j] = true;
					
						// std::cout << "pruned traj " << sort_order[j] <<std::endl;
						}
				}
			}
		}
		// debugging
		// for (size_t i=0; i<valid_inds.size();++i){
		// 	std::cout << valid_inds[i] << 't';
		// }
		// std::cout << std::endl;
		// if (valid_inds.size() < likelihood_order.size())
		// 	assert(0);
		return valid_inds;
	};

	// whether pedestrian has been updated in the past MAX_GAP_TIME seconds
	bool if_active(double current_time){
		// time_t cur_time = time(NULL);
		double time_elapsed = current_time - last_update_time;
		// std::cout << "time elapsed " << time_elapsed << std::endl;
		if ( time_elapsed > MAX_GAP_TIME){
			return false;
		}
		return true;
	};

	// convert size_t index to GP_TYPE for later plotting
	int orderInd2type(size_t ind){
		int gp_type = 0;
		switch ((int) ind){
			case 0:
				gp_type = GP_TYPE1;
				break;
			case 1:
				gp_type = GP_TYPE2;
				break;
			case 2:
				gp_type = GP_TYPE3;
				break;
			default:
				gp_type = GP_TYPE3;
				break;
		}
		return gp_type;
	};



};


#endif